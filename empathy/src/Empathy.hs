{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}

module Empathy
  ( empathy,
  )
where

import Codec.Archive.Zip as Zip
import Control.Applicative
import Control.Monad
import Control.Monad.Logger
import Data.Function
import Data.List (nub)
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding as TE
import qualified Database.Esqueleto.Legacy as E
import Database.Persist.Sqlite as DB
import Empathy.Constants
import Empathy.DB
import Empathy.DataDownload
import Empathy.HashedPassword
import Empathy.Image
import Empathy.Language
import Empathy.Static
import Empathy.Widget
import Lens.Micro
import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Client.TLS as Http
import System.Environment
import System.Exit
import System.Random.Shuffle
import Text.Hamlet
import Text.Read (readMaybe)
import Text.Shakespeare.I18N
import Text.Show.Pretty (ppShow)
import Yesod
import Yesod.Auth
import Yesod.EmbeddedStatic

-- The core datatype
data App = App
  { appApproot :: Maybe Text,
    appConnectionPool :: DB.ConnectionPool,
    appStatic :: EmbeddedStatic,
    appHttpManager :: Http.Manager,
    appGoogleAnalyticsTrackingId :: Maybe Text,
    appGoogleSearchConsoleVerificationId :: Maybe Text,
    appGoogleTagManagerId :: Maybe Text
  }

mkMessage "App" "messages" "en"

mkMessageFor "App" "StandardLanguage" "languages" "en"

mkYesod
  "App"
  [parseRoutes|

/ HomeR GET

/select-language/#WEMLanguage SelectLanguageR POST

/search SearchR GET POST

/about AboutR GET
/faq FaqR GET

/privacy-policy PrivacyPolicyR GET
/cookie-policy CookiePolicyR GET

/listener ListenerR GET
/photo/#PhotoId PhotoR GET
/delete-listener DeleteListenerR POST
/download-data DownloadDataR GET

/profile ProfileR GET POST
/set-availability/#Bool SetAvailabilityR POST

/auth AuthR Auth getAuth
/static StaticR EmbeddedStatic appStatic

|]

instance Yesod App where
  defaultLayout widget = do
    app <- getYesod
    pageContent <-
      widgetToPageContent $ do
        addStylesheetRemote "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.0/css/bulma.min.css"
        addStylesheetRemote
          "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
        toWidgetHead
          [hamlet|<link rel="icon" href=@{StaticR logo_favicon_ico} sizes="32x32" type="image/x-icon">|]
        [whamlet|
            ^{widget}
        |]
    withUrlRenderer
      [hamlet|
      $doctype 5
      <html>
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>#{pageTitle pageContent}

          <!-- Google search console -->
          $maybe searchConsoleId <- appGoogleSearchConsoleVerificationId app
            <meta name="google-site-verification" content="#{searchConsoleId}" />

          <!-- Google tag manager -->
          $maybe tagManagerId <- appGoogleTagManagerId app
            <script>
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','#{tagManagerId}');


          ^{pageHead pageContent}


          <!-- Google Analytics -->
          $maybe analyticsID <- appGoogleAnalyticsTrackingId app
            <script async src="https://www.googletagmanager.com/gtag/js?id=#{analyticsID}">
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', '#{analyticsID}');


        <body>

          $maybe tagManagerId <- appGoogleTagManagerId app
            <noscript>
              <iframe src="https://www.googletagmanager.com/ns.html?id=#{tagManagerId}"height="0" width="0" style="display:none;visibility:hidden">

          ^{pageBody pageContent}
    |]
  approot = ApprootMaster $ fromMaybe "" . appApproot
  authRoute _ = Just $ AuthR LoginR

instance YesodPersist App where
  type YesodPersistBackend App = SqlBackend
  runDB func = do
    pool <- getsYesod appConnectionPool
    DB.runSqlPool func pool

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

instance YesodAuth App where
  type AuthId App = UserId
  loginDest _ = ListenerR
  onLogin = pure ()
  logoutDest _ = ListenerR
  onLogout = pure ()
  authHttpManager = getsYesod appHttpManager
  authPlugins _ = [empathyAuthPlugin]
  authenticate = authenticateEmpathy
  authLayout = liftHandler . withNavBar

instance YesodAuthPersist App where
  type AuthEntity App = User

empathyAuthPluginName :: Text
empathyAuthPluginName = "empathy-auth"

empathyAuthPlugin :: AuthPlugin App
empathyAuthPlugin = AuthPlugin empathyAuthPluginName dispatch loginWidget
  where
    dispatch :: Text -> [Text] -> AuthHandler App TypedContent
    dispatch t p =
      case (t, p) of
        ("POST", ["login"]) -> postLoginR >>= sendResponse
        ("GET", ["register"]) -> getRegisterR >>= sendResponse
        ("POST", ["register"]) -> postRegisterR >>= sendResponse
        _ -> notFound
    loginWidget :: (Route Auth -> Route App) -> Widget
    loginWidget _ =
      [whamlet|
        <h1 .title .is-1>
          _{MsgLoginFormTitle}
        <form
          method="post"
          action=@{AuthR loginTargetR}>
          <div .field>
            <label .label>
              _{MsgEmailAddress}
            <div .control>
              <input type="email" name="email" required>
          <div .field>
            <label .label>
              _{MsgPassphrase}
            <div .control>
              <input type="password" name="password" required>
          <div .field>
            <div .control>
              <button .button .is-primary type="submit">
                _{MsgLoginFormAction}
        <p>
          _{MsgOr}
        <p>
          <a .button .is-secondary href=@{AuthR registerTargetR}>
            _{MsgRegistrationFormTitle}
      |]

loginTargetR :: AuthRoute
loginTargetR = PluginR empathyAuthPluginName ["login"]

data Login = Login
  { loginEmail :: Text,
    loginPassword :: Text
  }
  deriving (Show)

loginForm :: MonadAuthHandler App m => FormInput m Login
loginForm = Login <$> ireq emailField "email" <*> ireq passwordField "password"

postLoginR :: AuthHandler App TypedContent
postLoginR = do
  Login {..} <- runInputPost loginForm
  mUser <- liftHandler $ runDB $ getBy $ UniqueUserEmail loginEmail
  let err =
        liftHandler $ do
          setErrorMessageI
            "User not found"
            [ihamlet|
                _{MsgNoUserFoundWithEmailAndPassword loginEmail}
             |]
          redirect $ AuthR LoginR
  case mUser of
    Nothing -> err
    Just (Entity uid User {..}) ->
      if development || validatePassword userPassword loginPassword
        then liftHandler $ setLoginAs uid
        else err

registerTargetR :: AuthRoute
registerTargetR = PluginR empathyAuthPluginName ["register"]

data Registration = Registration
  { registrationEmail :: Text,
    registrationPassword1 :: Text,
    registrationPassword2 :: Text
  }
  deriving (Show)

registrationForm :: MonadAuthHandler App m => FormInput m Registration
registrationForm =
  Registration <$> ireq emailField "email" <*> ireq passwordField "password1"
    <*> ireq passwordField "password2"

getRegisterR :: AuthHandler App Html
getRegisterR = getRegisterPage Nothing

postRegisterR :: MonadAuthHandler App m => m Html
postRegisterR =
  withInputForm registrationForm getRegisterPage $ \Registration {..} ->
    liftHandler $ do
      unless (registrationPassword1 == registrationPassword2) $
        setErrorMessageI "Password mismatch" [ihamlet|_{MsgPasswordMismatch}|]
      mUser <- runDB $ getBy $ UniqueUserEmail registrationEmail
      case mUser of
        Just _ ->
          setErrorMessageI
            "User exists"
            [ihamlet|
            _{MsgEmailAlreadyExistsError registrationEmail}
          |]
        Nothing -> do
          mpwh <- liftIO $ passwordHash registrationPassword1
          case mpwh of
            Nothing -> setErrorMessageI "Hash Failure" [ihamlet|_{MsgHashFailure}|]
            Just pwh -> do
              uid <- runDB $ insert User {userEmail = registrationEmail, userPassword = pwh}
              void $ setLoginAs uid
      redirect ProfileR

getRegisterPage :: MonadAuthHandler App m => Maybe (FormResult Registration) -> m Html
getRegisterPage mfr = do
  let page =
        [whamlet|
          <h1 .title .is-1>
            _{MsgRegistrationFormTitle}
          <form
            method="post"
            action=@{AuthR registerTargetR}>
            <div .field>
              <label .label>
                _{MsgEmailAddress}
              <div .control>
                <input type="email" name="email" required>
            <div .field>
              <label .label>
                _{MsgPassphrase}
              <div .control>
                <input type="password" name="password1" required>
            <div .field>
              <label .label>
                _{MsgRepeatPassphrase}
              <div .control>
                <input type="password" name="password2" required>
            <p .help>
              _{MsgRegistrationHelp}
            <div .field>
              <div .control>
                <button .button .is-primary type="submit">
                  _{MsgRegistrationFormAction}
          <p>
            _{MsgOr}
          <p>
            <a .button .is-secondary href=@{AuthR LoginR}>
              _{MsgLoginFormTitle}
        |]
  liftHandler $ maybe withNavBar withFormResultNavBar mfr page

setLoginAs :: UserId -> Handler TypedContent
setLoginAs uid = setCredsRedirect $ Creds empathyAuthPluginName (T.pack $ show $ fromSqlKey uid) []

authenticateEmpathy ::
  (MonadHandler m, HandlerSite m ~ App) => Creds master -> m (AuthenticationResult App)
authenticateEmpathy creds =
  if credsPlugin creds == empathyAuthPluginName
    then do
      mec <- liftHandler $ loadUser $ credsIdent creds
      pure $
        case mec of
          Nothing ->
            ServerError ("Account not found: " <> T.pack (show (credsIdent creds))) :: AuthenticationResult App
          Just (Entity aid _) -> Authenticated aid
    else pure $ ServerError $ T.unwords ["Unknown authentication plugin: ", credsPlugin creds]

loadUser :: Text -> Handler (Maybe (Entity User))
loadUser key =
  runDB $
    case readMaybe (T.unpack key) of
      Nothing -> pure Nothing
      Just i64 -> do
        let k = toSqlKey i64 :: UserId
        r <- get $ toSqlKey i64
        pure $ Entity k <$> r

getHomeR :: Handler Html
getHomeR = do
  howItWorksWidget <- getHowItWorksWidget
  -- This could probably be done with a count, but oh well
  numberOfSpokenLanguages <-
    fmap length $ runDB $ E.select $ E.distinct $ E.from $ \listenerLanguage -> pure $ listenerLanguage E.^. ListenerLanguageLanguage
  withNavBar
    [whamlet|
      <div .has-text-centered>
        <section .hero>
          <div .hero-body>
            <div .container>
              <div .has-text-centered .is-centered style="max-width:400px;margin:auto">
                <figure .image .is-1by1>
                  <img src=@{StaticR logo_header_png}>

        <section .section>
          <div .columns .is-centered>
            <div .column .is-half>
              <p .content>
                _{MsgSpokenLanguages numberOfSpokenLanguages}
              <p .content>
                <a .button .is-medium .is-primary .is-fullwidth href=@{SearchR}>
                  _{MsgSearchListener}
              <p .content>
                <a .button .is-secondary href=@{FaqR}>
                  _{MsgIsThisForMeTitle}

          ^{howItWorksWidget}

        <section .section>

          <div .title .is-3>
            _{MsgDonate}

          <p>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
              <input type="hidden" name="cmd" value="_s-xclick" />
              <input type="hidden" name="hosted_button_id" value="XA4BZT7BAAKNU" />
              <input type="image" src="https://www.paypalobjects.com/en_US/BE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
              <img alt="" border="0" src="https://www.paypal.com/en_BE/i/scr/pixel.gif" width="1" height="1" />

          <div .subtitle .is-5>
            IBAN BE27 7350 5577 3873
            BLABLA FOUNDATION - WEM Gift

        <section .section>
          <h4 .title .is-2>
            _{MsgHowItWorksTitle}
          <p .content>
            _{MsgHowItWorksContents}

          <a .button .is-link href=@{AboutR}>
            _{MsgReadMore}

          <a .button .is-link href=@{StaticR press_kit_zip}>
            _{MsgPressKit}


        <section .section>
          <div .title .is-3>
            _{MsgWEMOnline}
          <p>
            <a .button .is-link href="https://www.facebook.com/worldwideempathyformedics/">
              Facebook

        <section .section>
          <div .title .is-4>
            _{MsgAcknowledgments}
          <div .columns>
            <div .column>
              <p .content>
                _{MsgWebsiteBy}
              <p .content>
                <div .has-text-centered .is-centered style="max-width:250px;margin:auto;">
                  <figure .image .is-4by1>
                    <a href="https://cs-syd.eu">
                      <img src=@{StaticR cs_syd_logo_png}>

            <div .column>
              <p .content>
                _{MsgLogoBy}
              <p .content>
                <div .has-text-centered .is-centered style="max-width:110px;margin:auto;">
                  <figure .image .is-3by2>
                    <a href="http://www.studio-mira.be">
                      <img src=@{StaticR studio_mira_logo_png}>
            <div .column>
              <p .content>
                _{MsgVisualsBy}
              <p .content>
                <div .has-text-centered .is-centered style="max-width:110px;margin:auto;">
                  <figure .image .is-5by6>
                    <a href="http://www.visfera.com">
                      <img src=@{StaticR visfera_logo_png}>
            <div .column>
              <p .content>
                _{MsgPrivacyBy}
              <p .content>
                <div .has-text-centered .is-centered style="max-width:300px;margin:auto;">
                  <figure .image .is-5by2>
                    <a href="https://www.dpo4you.net/">
                      <img src=@{StaticR dpo4you_logo_jpg}>
            <div .column>
              <p .content>
                _{MsgInCollaborationWith}
              <p .content>
                <div .has-text-centered .is-centered style="max-width:110px;margin:auto;">
                  <figure .image .is-5by6>
                    <a href="http://www.blabla-blabla.be">
                      <img src=@{StaticR blabla_logo_png}>

        <section .section>
          <div .title .is-4>
            _{MsgDisclaimerTitle}
          <p .content>
            _{MsgDisclaimerContents}

        <section .section>
          <div .title .is-3>
            _{MsgSponsors}
          <div .columns>
            <div .column>
              <div .has-text-centered style="max-width:75px;margin:auto;">
                <figure .image .is-square>
                  <a href="https://www.menarini.com">
                    <img src=@{StaticR sponsors_menarini_jpg}>
            <div .column>
              <div .has-text-centered style="max-width:100px;margin:auto;">
                <figure .image .is-5by4>
                  <a href="https://www.norgine.com">
                    <img src=@{StaticR sponsors_norgine_jpg}>
            <div .column>
              <div .has-text-centered style="max-width:100px;margin:auto;">
                <figure .image .is-3by2>
                  <a href="https://smblab.be">
                    <img src=@{StaticR sponsors_smb_png}>
          <div .columns>
            <div .column>
              <div .has-text-centered style="max-width:85px;margin:auto;">
                <figure .image .is-square>
                  <a href="https://bayer.com">
                    <img src=@{StaticR sponsors_bayer_png}>
            <div .column>
              <div .has-text-centered style="max-width:125px;margin:auto;">
                <figure .image .is-4by1>
                  <a href="https://chiesi.com">
                    <img src=@{StaticR sponsors_chiesi_png}>
            <div .column>
              <div .has-text-centered style="max-width:150px;margin:auto;">
                <figure .image .is-5by1>
                  <a href="https://www.biocodex.com">
                    <img src=@{StaticR sponsors_biocodex_png}>
          <br>
          <p .content>
            _{MsgContactUsInvitation}
          <a .button .is-text href="mailto:wem@blabla-blabla.be">
            _{MsgContactUs}
  |]

getHowItWorksWidget :: Handler Widget
getHowItWorksWidget = do
  lang <- getFirstMatchingSupportedLanguage
  pure
    [whamlet|
     <div .columns .is-centered>
        <div .column .is-full>
          <figure .image .is-5by1>
            <img src=@{StaticR $ frontMatterRoute lang 0}>
     <div .columns .is-centered>
        <div .column .is-one-third>
          <figure .image .is-3by5>
            <img src=@{StaticR $ frontMatterRoute lang 1}>
        <div .column .is-one-third>
          <figure .image .is-3by5>
            <img src=@{StaticR $ frontMatterRoute lang 2}>
        <div .column .is-one-third>
          <figure .image .is-3by5>
            <img src=@{StaticR $ frontMatterRoute lang 3}>
    |]

frontMatterRoute :: WEMLanguage -> Int -> Route EmbeddedStatic
frontMatterRoute l i =
  case l of
    WEMLangEnglish ->
      case i of
        0 -> home_0_en_png
        1 -> home_1_en_png
        2 -> home_2_en_png
        3 -> home_3_en_png
        _ -> error "No such route."
    WEMLangDutch ->
      case i of
        0 -> home_0_nl_png
        1 -> home_1_nl_png
        2 -> home_2_nl_png
        3 -> home_3_nl_png
        _ -> error "No such route."
    WEMLangGerman ->
      case i of
        0 -> home_0_de_png
        1 -> home_1_de_png
        2 -> home_2_de_png
        3 -> home_3_de_png
        _ -> error "No such route."
    WEMLangItalian ->
      case i of
        0 -> home_0_it_png
        1 -> home_1_it_png
        2 -> home_2_it_png
        3 -> home_3_it_png
        _ -> error "No such route."
    WEMLangFrench ->
      case i of
        0 -> home_0_fr_png
        1 -> home_1_fr_png
        2 -> home_2_fr_png
        3 -> home_3_fr_png
        _ -> error "No such route."

postSelectLanguageR :: WEMLanguage -> Handler Html
postSelectLanguageR lang = do
  setLanguage $ supportedLanguageAbbreviation lang
  setUltDestReferer
  redirectUltDest HomeR

getAboutR :: Handler Html
getAboutR = withNavBar $(widgetFile "about")

getFaqR :: Handler Html
getFaqR = withNavBar $(widgetFile "faq")

getPrivacyPolicyR :: Handler Html
getPrivacyPolicyR = withNavBar $(widgetFile "privacy-policy")

getCookiePolicyR :: Handler Html
getCookiePolicyR = withNavBar $(widgetFile "cookie-policy")

data Search = Search
  { searchLanguages :: Set StandardLanguage
  }

searchForm :: FormInput Handler Search
searchForm = Search <$> ireq languagesField "languages"

getSearchR :: Handler Html
getSearchR = getSearchPage Nothing

postSearchR :: Handler Html
postSearchR =
  withInputForm searchForm getSearchPage $ \Search {..} -> do
    -- TODO we can probably do this better with a join.
    listeners <- runDB $ selectList ([] :: [Filter Listener]) []
    listenersThatSpeakTheseLanguages <-
      fmap catMaybes $
        forM listeners $
          \(Entity lid _) -> do
            mls <- runDB $ selectList [ListenerLanguageListener ==. lid] []
            let listenerLanguages = S.fromList $ map (listenerLanguageLanguage . entityVal) mls
            -- TODO we can probably do this better with a join.
            pure $
              if S.null (S.intersection listenerLanguages searchLanguages)
                then Nothing
                else Just lid
    available <- runDB $ selectList [ListenerAvailable ==. True] []
    cards <-
      ((liftIO . shuffleM) =<<) $
        fmap catMaybes $
          forM available $
            \(Entity lid listener) -> do
              mls <- runDB $ selectList [ListenerLanguageListener ==. lid] []
              let listenerLanguages = S.fromList $ map (listenerLanguageLanguage . entityVal) mls
              pure $
                if S.null (S.intersection listenerLanguages searchLanguages)
                  then Nothing
                  else Just $ listenerCard listener listenerLanguages
    master <- getYesod
    langs <- languages
    let languageListStr = showLanguageList master langs searchLanguages
    withNavBar
      [whamlet|
        <h1 .title .is-1>
          _{MsgMatchingListeners}
        <h2 .subtitle .is-4>
          <p>
            <b>
              _{MsgListenersSearchLangAvail languageListStr (length cards) (length listenersThatSpeakTheseLanguages)}
          <p>
            _{MsgListenersMatchAvailableTotal (length available) (length listeners)}
        <p .content>
          _{MsgSelfResponsibility}
        <p .content>
          _{MsgVolunteeringNotice}
        <p .content>
          _{MsgInternationalCallsNotice}
        $forall card <- cards
          ^{card}
      |]

showLanguageList :: App -> [Text] -> Set StandardLanguage -> Text
showLanguageList master langs ls =
  let langStrings = flip map (S.toList ls) $ \l -> renderMessage master langs l
   in (("\"" <>) . (<> "\"")) $ T.intercalate ", " langStrings

getSearchPage :: Maybe (FormResult Search) -> Handler Html
getSearchPage mfr = do
  available <- runDB $ count [ListenerAvailable ==. True]
  listeners <- runDB $ count ([] :: [Filter Listener])
  ls <- languages
  liftIO $ print ls
  let defaultLanguages =
        nub $ mapMaybe (fmap supportedLanguageStandardLanguage . parseSupportedLanguage) ls
      languageSelected = (`elem` defaultLanguages)
  maybe
    withNavBar
    withFormResultNavBar
    mfr
    [whamlet|
      <h1 .title .is-1>
        _{MsgSearchListener}
      <h2 .subtitle .is-4>
        _{MsgListenersAvailableTotal available listeners}
      <form .form
        method=post
        action=@{SearchR}>
        ^{languagesForm languageSelected}
        <button .button .is-primary type="submit">
          _{MsgSearch}
    |]

listenerCard :: Listener -> Set StandardLanguage -> Widget
listenerCard listener languages =
  [whamlet|
    <div .level .card>
      <div .card-content>
        <div .media>
          $maybe pid <- listenerPhoto listener
            <div .media-left>
              <img style="max-width:128px;max-height:128px;width:auto;height:auto;" src=@{PhotoR pid} alt="Photo of #{listenerName listener}">
          <div .media-content>
            <p .title .is-4>
              <i .icon .is-small .fa .fa-user>
              #{listenerName listener}
            <p .subtitle .is-6>
              $maybe professional <- listenerProfessional listener
                <p>
                  <i .icon .is-small  .fa .fa-user-md>
                  _{MsgProfessional professional}
              $if listenerNvcTrainer listener
                <p>
                  <i .icon .is-small  .fa .fa-comments>
                  _{MsgNVCTrainer}
              $maybe country <- listenerCountry listener
                <p>
                  <i .icon .is-small  .fa .fa-flag>
                  _{MsgCountry country}
              _{MsgLanguages}:
              <ul>
                $forall language <- languages
                  <li>
                      _{language}
                      /
                      #{standardLanguageNative language}

        <div .content>
          $maybe email <- listenerEmail listener
            <p>
              <i .icon .is-small .fa .fa-at>
              _{MsgEmailAddress}:
              <a href="mailto:#{email}">
                #{email}
          $maybe phone <- listenerPhone listener
            <p>
              <i .icon .is-small .fa .fa-phone>
              _{MsgPhoneNumber}:
              <a href="tel:#{phone}">
                #{phone}
            $if listenerInternationalCalls listener
              <p>
                <i .icon .is-small .fa .fa-check>
                _{MsgInternationalCalls}
          $maybe whatsapp <- listenerWhatsapp listener
            <p>
              <i .icon .is-small .fa .fa-whatsapp>
              Whatsapp: #{whatsapp}
          $maybe signal <- listenerSignal listener
            <p>
              <i .icon .is-small .fa .fa-comment>
              Signal: #{signal}
          $maybe telegram <- listenerTelegram listener
            <p>
              <i .icon .is-small .fa .fa-telegram>
              Telegram: #{telegram}
          $maybe skype <- listenerSkype listener
            <p>
              <i .icon .is-small .fa .fa-skype>
              Skype:
              <a href="skype:#{skype}?call">
                #{skype}
          $maybe ghangouts <- listenerGhangouts listener
            <p>
              <i .icon .is-small .fa .fa-google>
              Google Hangouts: #{ghangouts}
  |]

getListenerR :: Handler Html
getListenerR = do
  mid <- maybeAuthId
  mle <- runDB $ fmap join $ forM mid $ \uid -> getBy $ UniqueListenerUser uid
  liftIO $ print mid
  withNavBar
    [whamlet|
      <div>
        <section .hero>
          <div .hero-body>
            <div .container>
              <div .has-text-centered .is-centered style="max-width:400px;margin:auto">
                <figure .image .is-1by1>
                  <img src=@{StaticR logo_header_png}>

        <section .section .has-text-centered>
          $maybe _ <- mid
            $maybe Entity _ listener <- mle
              <p .content>
                _{MsgListenerResponsibilityNote}
              <div .columns .is-centered>
                <div .column>
                  $if listenerAvailable listener
                    <p .content .has-text-success .has-text-weight-bold .title .is-3>
                      _{MsgAvailable}
                    <form method=post action=@{SetAvailabilityR False}>
                      <button .button .is-danger type="submit">
                        _{MsgMarkUnavailable}
                  $else
                    <p .content .has-text-danger .has-text-weight-bold .title .is-3>
                      _{MsgUnavailable}
                    <form method=post action=@{SetAvailabilityR True}>
                      <button .button .is-success type="submit">
                        _{MsgMarkAvailable}
            <div .columns .is-centered>
              <div .column .is-half>
                <a .button .is-large .is-primary .is-fullwidth href=@{ProfileR}>
                  _{MsgProfilePageTitle}
            <div .columns .is-centered>
              <div .column .is-half>
                <a .button .is-secondary href=@{AuthR LogoutR} .is-warning>
                  _{MsgLogOutAction}
          $nothing
            <div .columns .is-centered>
              <div .column .is-half>
                <a .button .is-large .is-primary .is-fullwidth href=@{AuthR LoginR}>
                  _{MsgLoginFormTitle}
            <div .columns .is-centered>
              <div .column .is-half>
                <a .button .is-secondary href=@{AuthR registerTargetR} .is-warning>
                  _{MsgRegistrationFormTitle}
            <div .columns .is-centered>
              <div .column .is-half>
                 <a .button .is-secondary href=@{HomeR}>
                   _{MsgBackHome}
        $maybe Entity _ _ <- mle
          <section .section>
            <b>
              _{MsgSuggestions1}

            <p>
              _{MsgSuggestions2}

            <p>
              _{MsgGoogleFormInvitation}
            <p>
              <a .button .is-link href="https://docs.google.com/forms/d/e/1FAIpQLSe-2syrDperCv3UfwQ5fILAYRVE6RWoPlM6g7hG_ZVroYiTdw/viewform">
                Google form
        <section .section .has-text-centered>
          <div .columns .is-centered>
            <div .column .is-half>
              <form
                method="post"
                action=@{DeleteListenerR}>
                <button .button .is-danger
                  onclick="return confirm('_{MsgAccountDeletionDoubleConfirmation}');">
                  _{MsgDeleteAccountAction}
          <div .columns .is-centered>
            <div .column .is-half>
              <a .button .is-danger href=@{DownloadDataR}>
                _{MsgDownloadDataAction}
    |]

postDeleteListenerR :: Handler Html
postDeleteListenerR = do
  uid <- requireAuthId
  runDB $ do
    ml <- getBy $ UniqueListenerUser uid
    forM_ ml $ \(Entity lid _) -> do
      deleteWhere [ListenerLanguageListener ==. lid]
      deleteWhere [PhotoListener ==. lid]
      delete lid
    delete uid
  redirect ListenerR

getDownloadDataR :: Handler TypedContent
getDownloadDataR = do
  uid <- requireAuthId
  archive <- runDB $ makeDataZip uid
  addHeader "Content-Disposition" $ T.concat ["attachment; filename=\"", "wem-data.zip", "\""]
  sendResponse (TE.encodeUtf8 "application/zip", toContent $ Zip.fromArchive archive)

withFormResultNavBar :: FormResult a -> WidgetFor App () -> HandlerFor App Html
withFormResultNavBar fr w =
  case fr of
    FormSuccess _ -> withNavBar w
    FormFailure ts -> withFormFailureNavBar ts w
    FormMissing -> withFormFailureNavBar ["Missing data"] w

withNavBar :: WidgetFor App () -> HandlerFor App Html
withNavBar = withFormFailureNavBar []

data WEMLanguage
  = WEMLangEnglish
  | WEMLangDutch
  | WEMLangFrench
  | WEMLangGerman
  | WEMLangItalian
  deriving (Show, Read, Eq, Enum, Bounded)

instance PathPiece WEMLanguage where
  fromPathPiece = parseSupportedLanguage
  toPathPiece = supportedLanguageAbbreviation

supportedLanguages :: [WEMLanguage]
supportedLanguages = [minBound .. maxBound]

parseSupportedLanguage :: Text -> Maybe WEMLanguage
parseSupportedLanguage =
  \case
    "en" -> Just WEMLangEnglish
    "nl" -> Just WEMLangDutch
    "fr" -> Just WEMLangFrench
    "de" -> Just WEMLangGerman
    "it" -> Just WEMLangItalian
    _ -> Nothing

supportedLanguageAbbreviation :: WEMLanguage -> Text
supportedLanguageAbbreviation =
  \case
    WEMLangEnglish -> "en"
    WEMLangDutch -> "nl"
    WEMLangFrench -> "fr"
    WEMLangGerman -> "de"
    WEMLangItalian -> "it"

supportedLanguageNative :: WEMLanguage -> Text
supportedLanguageNative =
  \case
    WEMLangEnglish -> "English"
    WEMLangDutch -> "Nederlands"
    WEMLangFrench -> "Français"
    WEMLangGerman -> "Deutsch"
    WEMLangItalian -> "Italiano"

supportedLanguageStandardLanguage :: WEMLanguage -> StandardLanguage
supportedLanguageStandardLanguage =
  \case
    WEMLangEnglish -> LangEnglish
    WEMLangDutch -> LangDutch
    WEMLangFrench -> LangFrench
    WEMLangGerman -> LangGerman
    WEMLangItalian -> LangItalian

getFirstMatchingSupportedLanguage :: Handler WEMLanguage
getFirstMatchingSupportedLanguage = do
  ls <- languages
  pure $ fromMaybe WEMLangEnglish $ firstMatchingSupportedLanguage ls

firstMatchingSupportedLanguage :: [Text] -> Maybe WEMLanguage
firstMatchingSupportedLanguage =
  \case
    [] -> Nothing
    (l : ls) -> parseSupportedLanguage l <|> firstMatchingSupportedLanguage ls

withFormFailureNavBar :: [Text] -> WidgetFor App () -> HandlerFor App Html
withFormFailureNavBar formFailureMsgs body = do
  msgs <- getMessages
  defaultLayout $(widgetFile "navbar")

languagesForm :: (StandardLanguage -> Bool) -> Widget
languagesForm languageSelected =
  [whamlet|
    <div .field>
      <label .label>
        _{MsgLanguages}

      <div .control>
        <div .select .is-multiple>
          <select multiple size="8" required name="languages">
            $forall language <- allStandardLanguages
              <option value="#{standardLanguageText language}" :languageSelected language:selected>
                _{language} / #{standardLanguageNative language}

      <p .help>
        _{MsgHoldCtrl}
        _{MsgForgiveTranslations}
  |]

languagesField :: Field Handler (Set StandardLanguage)
languagesField =
  checkMMap (pure . (Right :: a -> Either Text a) . S.fromList) S.toList $ multiSelectField list
  where
    list :: Handler (OptionList StandardLanguage)
    list = do
      master <- getYesod
      langs <- languages
      pure $ languagesOptionList $ renderMessage master langs

languagesOptionList :: (StandardLanguage -> Text) -> OptionList StandardLanguage
languagesOptionList translateLang =
  OptionList {olOptions = map mkOption allStandardLanguages, olReadExternal = parseStandardLanguage}
  where
    mkOption :: StandardLanguage -> Option StandardLanguage
    mkOption sl =
      Option
        { optionDisplay = translateLang sl <> " / " <> standardLanguageNative sl,
          optionInternalValue = sl,
          optionExternalValue = standardLanguageText sl
        }

getSignupThankyouR :: Handler Html
getSignupThankyouR =
  withNavBar
    [whamlet|
    <h1 .title>
      Thank you for signing up.
    <a .button .is-link href=@{HomeR}>
      Back
  |]

getPhotoR :: PhotoId -> Handler TypedContent
getPhotoR pid = do
  photo <- runDB $ get404 pid
  respond (photoContentType photo) (photoData photo)

data Profile = Profile
  { profileName :: Text,
    profileAvailable :: Bool,
    profileCountry :: Maybe Text,
    profileLanguages :: Set StandardLanguage,
    profileEmail :: Maybe Text,
    profilePhone :: Maybe Text,
    profileInternationalCalls :: Bool,
    profileWhatsapp :: Maybe Text,
    profileSignal :: Maybe Text,
    profileTelegram :: Maybe Text,
    profileSkype :: Maybe Text,
    profileGhangouts :: Maybe Text,
    profileNvcTrainer :: Bool,
    profileProfessional :: Maybe Text,
    profilePhoto :: Maybe FileInfo
  }

profileForm :: FormInput Handler Profile
profileForm =
  Profile <$> ireq textField "name" <*> ireq checkBoxField "available" <*> iopt textField "country"
    <*> ireq languagesField "languages"
    <*> iopt emailField "email"
    <*> iopt textField "phone"
    <*> ireq checkBoxField "international-calls"
    <*> iopt textField "whatsapp"
    <*> iopt textField "signal"
    <*> iopt textField "telegram"
    <*> iopt textField "skype"
    <*> iopt textField "ghangouts"
    <*> ireq checkBoxField "nvc"
    <*> iopt textField "professional"
    <*> iopt fileField "photo"

allowedImagesTypes :: [Text]
allowedImagesTypes = ["image/jpeg", "image/png"]

getProfileR :: Handler Html
getProfileR = getProfilePage Nothing

postProfileR :: Handler Html
postProfileR =
  withInputForm profileForm getProfilePage $ \Profile {..} -> do
    userId <- requireAuthId
    case profilePhone <|> profileWhatsapp <|> profileSignal <|> profileTelegram <|> profileSkype
      <|> profileGhangouts of
      Nothing -> invalidArgsI [MsgAtLeastOneVoiceOptionError]
      Just _ ->
        runDB $ do
          Entity lid _ <-
            upsertBy
              (UniqueListenerUser userId)
              Listener
                { listenerUser = userId,
                  listenerAvailable = profileAvailable,
                  listenerName = profileName,
                  listenerCountry = profileCountry,
                  listenerEmail = profileEmail,
                  listenerPhone = profilePhone,
                  listenerInternationalCalls = profileInternationalCalls,
                  listenerWhatsapp = profileWhatsapp,
                  listenerSignal = profileSignal,
                  listenerTelegram = profileTelegram,
                  listenerSkype = profileSkype,
                  listenerGhangouts = profileGhangouts,
                  listenerVolunteer = True,
                  listenerNvcTrainer = profileNvcTrainer,
                  listenerProfessional = profileProfessional,
                  listenerPhoto = Nothing
                }
              [ ListenerAvailable =. profileAvailable,
                ListenerName =. profileName,
                ListenerCountry =. profileCountry,
                ListenerEmail =. profileEmail,
                ListenerPhone =. profilePhone,
                ListenerInternationalCalls =. profileInternationalCalls,
                ListenerWhatsapp =. profileWhatsapp,
                ListenerSignal =. profileSignal,
                ListenerTelegram =. profileTelegram,
                ListenerSkype =. profileSkype,
                ListenerGhangouts =. profileGhangouts,
                ListenerNvcTrainer =. profileNvcTrainer,
                ListenerProfessional =. profileProfessional
              ]
          deleteWhere [ListenerLanguageListener ==. lid]
          forM_ profileLanguages $ \language ->
            insert_
              ListenerLanguage {listenerLanguageListener = lid, listenerLanguageLanguage = language}
          forM_ profilePhoto $ \fi -> do
            unless (fileContentType fi `elem` allowedImagesTypes) $
              invalidArgsI
                [ MsgFileTypeNotAllowed (fileContentType fi),
                  MsgAllowedFileTypes $ T.pack $ show allowedImagesTypes
                ]
            let contentType = TE.encodeUtf8 $ fileContentType fi
            photoBS <- fileSourceByteString fi
            case wemCropImage photoBS of
              Left err -> invalidArgsI [MsgImageProcessingProblem $ T.pack err]
              Right photoBS' -> do
                Entity tpid _ <-
                  upsertBy
                    (UniqueListenerPhoto lid)
                    Photo
                      { photoListener = lid,
                        photoContentType = contentType,
                        photoData = photoBS'
                      }
                    [PhotoContentType =. "image/png", PhotoData =. photoBS']
                update lid [ListenerPhoto =. Just tpid]
    redirect ProfileR

getProfilePage :: Maybe (FormResult Profile) -> Handler Html
getProfilePage mfr = do
  userId <- requireAuthId
  mle <- runDB $ getBy $ UniqueListenerUser userId
  let mlid = entityKey <$> mle
  mls <-
    fmap (fromMaybe []) $
      forM mlid $
        \lid -> runDB $ selectList [ListenerLanguageListener ==. lid] []
  let ml = entityVal <$> mle
  _ <- fmap join $ forM mlid $ \lid -> runDB $ getBy $ UniqueListenerPhoto lid
  let listenerLanguages = S.fromList $ map (listenerLanguageLanguage . entityVal) mls
      languageSelected :: StandardLanguage -> Bool
      languageSelected language = language `S.member` listenerLanguages
      availableSelected = maybe False listenerAvailable ml
      nvcTrainerSelected = maybe False listenerNvcTrainer ml
      internationalCallsSelected = maybe False listenerInternationalCalls ml
  maybe
    withNavBar
    withFormResultNavBar
    mfr
    [whamlet|
       <section .section>
         <h1 .title .is-1>
           Listener Profile
       $maybe listener <- ml
         <section .section>
           <h2 .title .is-2>
             _{MsgProfileCardTitle}
           <p>
             _{MsgProfileCardPreview}
             ^{listenerCard listener listenerLanguages}
             $if maybe False listenerAvailable ml
               _{MsgAvailable}
             $else
               _{MsgUnavailable}

       <section .section>
         $maybe _ <- ml
           <h2 .title .is-2>
             _{MsgUpdateProfileTitle}
         $nothing
           <p>
             _{MsgProfileNotVisibleYet}
             _{MsgInfoChangableNotice}
           <h2 .title .is-2>
             _{MsgSetupProfileTitle}
         <p>
           <form method=post action=@{ProfileR} enctype="multipart/form-data">
             <div .field>
               <label .label>
                 _{MsgNameLabel}
               <div .control>
                 <input .input .is-medium name="name" type="text" placeholder="_{MsgNameLabel}" required value="#{maybe "" listenerName ml}">
             ^{languagesForm languageSelected}
             <div .field>
               <label .label>
                  _{MsgCountryLabel}
               <div .control .has-icons-left>
                 <input .input type="text" name="country" placeholder="_{MsgCountryLabel}" value="#{fromMaybe "" $ ml >>= listenerCountry}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-flag>

             <div .field>
               <label .label>
                 _{MsgProfessionalListenerLabel}
               <div .control .has-icons-left>
                 <input .input type="text" name="professional" value="#{fromMaybe "" $ ml >>= listenerProfessional}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-user-md>
               <p .help>
                 _{MsgProfessionalListenerHelp}

             <div .field>
               <label .label>
                 <i .icon .is-small .fa .fa-comments>
                 _{MsgNVCTrainerLabel}
               <div .control .has-icons-left>
                 <input type="checkbox" name="nvc" :nvcTrainerSelected:checked>
                 <span .icon .is-small .is-left>

             <div .field>
               <label .label>
                 _{MsgPictureLabel}
               <div .file>
                 <label .file-label>
                   <input .file-input type="file" name="photo">
                   <span .file-cta>
                     <span .file-icon>
                       <i .fa .fa-upload>
                     <span .file-label>
                        _{MsgPictureUploadLabel}


             <div .field>
               <label .label>
                 _{MsgContactLabel}
               <p .help>
                 _{MsgContactHelp}

             <div .field>
               <label .label>
                 _{MsgEmailLabel}
               <div .control .has-icons-left>
                 <input .input type="email" placeholder="_{MsgEmailLabel}" name="email" value="#{fromMaybe "" $ ml >>= listenerEmail}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-at>

             <div .field>
               <label .label>
                 _{MsgPhoneCallLabel}
               <div .control .has-icons-left>
                 <input .input type="tel" placeholder="_{MsgPhoneNumberPlaceholder}" name="phone" value="#{fromMaybe "" $ ml >>= listenerPhone}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-phone>

             <div .field>
               <label .label>
                 _{MsgInternationalPhonecallsLabel}
               <div .control .has-icons-left>
                 <input type="checkbox" name="international-calls" :internationalCallsSelected:checked>

             <div .field>
               <label .label>
                 Whatsapp
               <div .control .has-icons-left>
                 <input .input type="text" placeholder="Whatsapp" name="whatsapp" value="#{fromMaybe "" $ ml >>= listenerWhatsapp}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-whatsapp>
               <p .help>
                 _{MsgCountryCodeHelp}

             <div .field>
               <label .label>
                 Signal
               <div .control .has-icons-left>
                 <input .input type="text" placeholder="Signal" name="signal" value="#{fromMaybe "" $ ml >>= listenerSignal}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-comment>

             <div .field>
               <label .label>
                 Telegram
               <div .control .has-icons-left>
                 <input .input type="text" placeholder="Telegram" name="telegram" value="#{fromMaybe "" $ ml >>= listenerTelegram}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-telegram>

             <div .field>
               <label .label>
                 Skype
               <div .control .has-icons-left>
                 <input .input type="text" placeholder="Skype" name="skype" value="#{fromMaybe "" $ ml >>= listenerSkype}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-skype>

             <div .field>
               <label .label>
                 Google hangouts
               <div .control .has-icons-left>
                 <input .input type="text" placeholder="Google Hangouts" name="ghangouts" value="#{fromMaybe "" $ ml >>= listenerGhangouts}">
                 <span .icon .is-small .is-left>
                   <i .fa .fa-google>

             <div .field>
               <label .label>
                 <i .icon .is-small .fa .fa-users>
                 _{MsgAvailableLabel}
               <div .control .has-icons-left>
                 <input type="checkbox" name="available" :availableSelected:checked>
               <p .help>
                 _{MsgAvailableHelp}

             <div .field>
               <label .label>
                 _{MsgChecklistLabel}
               <div .control>
                 <input type="checkbox" required :isJust ml:checked>

             <div .field>
               <div .control>
                 <button .button .is-primary>
                   _{MsgSaveProfileSettings}
             <div .field>
               <div .control>
                 <a .button .is-secondary href=@{ListenerR}>
                   _{MsgBackToListener}
             <div .field>
               <div .control>
                 <a .button .is-secondary href=@{HomeR}>
                   _{MsgBackHome}

    |]

postSetAvailabilityR :: Bool -> Handler Html
postSetAvailabilityR b = do
  u <- requireAuthId
  ml <- runDB $ getBy $ UniqueListenerUser u
  forM_ ml $ \(Entity lid _) -> runDB $ update lid [ListenerAvailable =. b]
  redirect ListenerR

withInputForm ::
  forall m a result.
  MonadHandler m =>
  FormInput m a ->
  (Maybe (FormResult a) -> m result) ->
  (a -> m result) ->
  m result
withInputForm form getPage func = do
  fr <- runInputPostResult form
  case fr of
    FormSuccess res -> func res
    _ -> getPage (Just fr)

setErrorMessage :: MonadHandler m => Html -> Html -> m ()
setErrorMessage msg body =
  setMessage
    [shamlet|
      <article .message .is-danger>
        <div .message-header>
          #{msg}
        <div .message-body>
          #{body}
  |]

setErrorMessageI :: RenderMessage App msg => Html -> HtmlUrlI18n msg (Route App) -> Handler ()
setErrorMessageI m expr = do
  master <- getYesod
  langs <- languages
  renderUrl <- getUrlRenderParams
  setErrorMessage m (expr (toHtml . renderMessage master langs) renderUrl :: Html)

data Settings = Settings
  { setHost :: String,
    setPort :: Int,
    setDBFile :: FilePath,
    setApproot :: Maybe Text,
    setGoogleAnalyticsTrackingId :: Maybe Text,
    setGoogleSearchConsoleVerificationId :: Maybe Text,
    setGoogleTagManagerId :: Maybe Text
  }
  deriving (Show)

getSettings :: IO Settings
getSettings = do
  env <- getEnvironment
  let v k = lookup ("EMPATHY_" <> k) env
      r k =
        forM (v k) $ \s ->
          case readMaybe s of
            Nothing -> die $ "Un-read-able value: " <> s
            Just r -> pure r
  let setHost = fromMaybe "localhost" $ v "HOST"
  setPort <- fromMaybe 8080 <$> r "PORT"
  let setDBFile = fromMaybe "empathy.sqlite3" $ v "DATABASE"
      setApproot = T.pack <$> v "APPROOT"
      setGoogleAnalyticsTrackingId = T.pack <$> v "GOOGLE_ANALYTICS_TRACKING"
      setGoogleSearchConsoleVerificationId = T.pack <$> v "GOOGLE_SEARCH_CONSOLE_VERIFICATION"
      setGoogleTagManagerId = T.pack <$> v "GOOGLE_TAG_MANAGER"
  pure Settings {..}

empathy :: IO ()
empathy = do
  s@Settings {..} <- getSettings
  runStderrLoggingT $
    DB.withSqlitePoolInfo (DB.mkSqliteConnectionInfo (T.pack setDBFile) & DB.fkEnabled .~ False) 1 $
      \appConnectionPool -> do
        when development $
          logInfoN $
            T.unlines
              [ T.unwords
                  [ "Serving the empathy web service at",
                    T.concat ["http://" <> T.pack setHost <> ":" <> T.pack (show setPort)]
                  ],
                "with settings",
                T.pack (ppShow s)
              ]
        void $ DB.runSqlPool (DB.runMigration migrateAll >> appSpecificMigration) appConnectionPool
        let appApproot = setApproot
            appStatic = empathyStatic
        appHttpManager <- liftIO $ Http.newManager Http.tlsManagerSettings
        let appGoogleAnalyticsTrackingId = setGoogleAnalyticsTrackingId
            appGoogleSearchConsoleVerificationId = setGoogleSearchConsoleVerificationId
            appGoogleTagManagerId = setGoogleTagManagerId
        liftIO $ Yesod.warp setPort App {..}

appSpecificMigration :: SqlPersistT (LoggingT IO) ()
appSpecificMigration = do
  photos <- selectList [] []
  forM_ photos $ \(Entity pid Photo {..}) ->
    case wemCropImage photoData of
      Left err -> do
        logWarnNS "MIGRATION" $
          T.pack $
            unwords
              ["Failed to convert image", show $ fromSqlKey pid, "in db: " <> err, ", deleting it."]
        delete pid
      Right converted ->
        unless (photoData == converted) $
          update pid [PhotoData =. converted, PhotoContentType =. "image/png"]
