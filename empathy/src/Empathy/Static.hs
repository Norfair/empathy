{-# LANGUAGE TemplateHaskell #-}

module Empathy.Static where

import Empathy.Constants
import Yesod.EmbeddedStatic

mkEmbeddedStatic development "empathyStatic" $
  let f fp = embedFileAt fp ("static/" ++ fp)
   in concat
        [ [ f "logo/header.png",
            f "logo/favicon.ico",
            f "studio-mira/logo.png",
            f "cs-syd/logo.png",
            f "visfera/logo.png",
            f "dpo4you/logo.jpg",
            f "blabla/logo.png",
            f "sponsors/bayer.png",
            f "sponsors/biocodex.png",
            f "sponsors/chiesi.png",
            f "sponsors/menarini.jpg",
            f "sponsors/norgine.jpg",
            f "sponsors/smb.png",
            f "press/kit.zip"
          ],
          flip concatMap ["de", "en", "fr", "it", "nl"] $ \l ->
            map (\i -> f ("home/" ++ show i ++ "_" ++ l ++ ".png")) [0 .. (3 :: Int)],
          map (\i -> f ("team/image" ++ show i ++ ".jpg")) [1 .. (12 :: Int)]
        ]
