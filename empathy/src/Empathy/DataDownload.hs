{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Empathy.DataDownload where

import Codec.Archive.Zip as Zip
import Control.Monad.IO.Class
import qualified Data.ByteString.Lazy as LB
import Data.Csv as CSV
import Data.Foldable
import Data.Text (Text)
import qualified Data.Text as T
import Database.Persist
import Database.Persist.Sql
import Empathy.DB

makeDataZip :: MonadIO m => UserId -> SqlPersistT m Archive
makeDataZip uid = do
  ml <- getBy $ UniqueListenerUser uid
  csvs <-
    sequence $
      concat
        [ [ getCsvFor [UserId ==. uid],
            getCsvFor [ListenerUser ==. uid]
          ],
          maybe
            []
            ( \(Entity lid _) ->
                [ getCsvFor [ListenerLanguageListener ==. lid],
                  getCsvFor [PhotoListener ==. lid]
                ]
            )
            ml
        ]
  let archive =
        foldl' (flip addEntryToArchive) emptyArchive (map (\(fp, bs) -> toEntry fp 0 bs) csvs)
  pure archive

getCsvFor ::
  forall record m.
  ( MonadIO m,
    PersistEntity record,
    PersistEntityBackend record ~ SqlBackend,
    ToBackendKey SqlBackend record,
    Show record
  ) =>
  [Filter record] ->
  SqlPersistT m (FilePath, LB.ByteString)
getCsvFor fs = do
  values <- selectList fs [Asc (persistIdField :: EntityField record (Key record))]
  let def = entityDef (pure undefined :: IO record)
  let pvalues =
        map
          ( \(Entity i r) ->
              tshow (fromSqlKey i) : map (fieldText . toPersistValue) (toPersistFields r)
          )
          values
  pure (T.unpack (unEntityNameDB $ getEntityDBName def) ++ ".csv", encode pvalues)

fieldText :: PersistValue -> Text
fieldText (PersistText t) = t
fieldText (PersistByteString bs) = tshow bs
fieldText (PersistInt64 i) = tshow i
fieldText (PersistDouble d) = tshow d
fieldText (PersistRational r) = tshow r
fieldText (PersistBool b) = tshow b
fieldText (PersistDay d) = tshow d
fieldText (PersistTimeOfDay tod) = tshow tod
fieldText (PersistUTCTime utct) = T.pack $ show utct
fieldText PersistNull = ""
fieldText (PersistArray ls) = tshow ls
fieldText (PersistList ls) = tshow ls
fieldText (PersistMap m) = tshow m
fieldText (PersistObjectId bs) = tshow bs
fieldText (PersistLiteral_ _ bs) = tshow bs

tshow :: Show v => v -> Text
tshow = T.pack . show
