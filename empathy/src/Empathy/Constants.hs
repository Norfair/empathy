{-# LANGUAGE CPP #-}

module Empathy.Constants where

development :: Bool

#ifdef DEVELOPMENT
development = True
#else
development = False
#endif
