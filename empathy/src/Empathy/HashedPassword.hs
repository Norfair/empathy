{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Empathy.HashedPassword
  ( passwordHash,
    HashedPassword (),
    validatePassword,
  )
where

import qualified Crypto.BCrypt as BCrypt
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.Text.Encoding as TE
import Database.Persist.Sql
import GHC.Generics (Generic)

newtype HashedPassword
  = HashedPassword ByteString
  deriving (Show, Eq, Ord, Read, Generic, PersistField, PersistFieldSql)

hashingpolicy :: BCrypt.HashingPolicy
hashingpolicy = BCrypt.fastBcryptHashingPolicy

passwordHash :: Text -> IO (Maybe HashedPassword)
passwordHash =
  fmap (fmap HashedPassword) . BCrypt.hashPasswordUsingPolicy hashingpolicy . TE.encodeUtf8

validatePassword :: HashedPassword -> Text -> Bool
validatePassword (HashedPassword hp) t = BCrypt.validatePassword hp $ TE.encodeUtf8 t
