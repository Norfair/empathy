{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Empathy.Language where

import Data.Proxy
import Data.Text (Text)
import Database.Persist
import Database.Persist.Sql
import Empathy.StandardOrRaw
import GHC.Generics (Generic)

data StandardLanguage
  = LangEnglish
  | LangArabic
  | LangBengali
  | LangCantonese
  | LangDanish
  | LangDutch
  | LangFinnish
  | LangFrench
  | LangGerman
  | LangGreek
  | LangHebrew
  | LangHindi
  | LangHungarian
  | LangItalian
  | LangJapanese
  | LangJavanese
  | LangKannada
  | LangKorean
  | LangLahnda
  | LangMacedonian
  | LangMandarin
  | LangMarathi
  | LangNorwegian
  | LangPolish
  | LangPortuguese
  | LangRomanian
  | LangRussian
  | LangSpanish
  | LangSwedish
  | LangTamil
  | LangTelegu
  | LangTurkish
  | LangUrdu
  | LangVietnamese
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

-- From https://www.omniglot.com/language/names.htm
instance PersistField StandardLanguage where
  toPersistValue = toPersistValue . standardLanguageText
  fromPersistValue v = do
    t <- fromPersistValue v
    case parseStandardLanguage t of
      Nothing -> Left $ "Un-parseable language: " <> t
      Just l -> pure l

instance PersistFieldSql StandardLanguage where
  sqlType Proxy = sqlType (Proxy :: Proxy Text)

allStandardLanguages :: [StandardLanguage]
allStandardLanguages = [minBound .. maxBound]

standardLanguageText :: StandardLanguage -> Text
standardLanguageText r =
  case r of
    LangEnglish -> "english"
    LangArabic -> "arabic"
    LangBengali -> "bengali"
    LangCantonese -> "cantonese"
    LangDutch -> "dutch"
    LangFrench -> "french"
    LangGerman -> "german"
    LangHindi -> "hindi"
    LangHebrew -> "hebrew"
    LangJapanese -> "japanese"
    LangJavanese -> "javanese"
    LangKannada -> "kannada"
    LangKorean -> "korean"
    LangLahnda -> "lahnda"
    LangMandarin -> "mandarin"
    LangMarathi -> "marathi"
    LangPortuguese -> "portuguese"
    LangRussian -> "russian"
    LangSpanish -> "spanish"
    LangTamil -> "tamil"
    LangTelegu -> "telegu"
    LangTurkish -> "turkish"
    LangUrdu -> "urdu"
    LangVietnamese -> "vietnamese"
    LangPolish -> "polish"
    LangDanish -> "danish"
    LangItalian -> "italian"
    LangMacedonian -> "macedonian"
    LangGreek -> "greek"
    LangFinnish -> "finnish"
    LangSwedish -> "swedish"
    LangNorwegian -> "norwegian"
    LangRomanian -> "romanian"
    LangHungarian -> "hungarian"

parseStandardLanguage :: Text -> Maybe StandardLanguage
parseStandardLanguage t =
  case t of
    "arabic" -> Just LangArabic
    "bengali" -> Just LangBengali
    "cantonese" -> Just LangCantonese
    "danish" -> Just LangDanish
    "dutch" -> Just LangDutch
    "english" -> Just LangEnglish
    "finnish" -> Just LangFinnish
    "french" -> Just LangFrench
    "german" -> Just LangGerman
    "greek" -> Just LangGreek
    "hindi" -> Just LangHindi
    "hebrew" -> Just LangHebrew
    "hungarian" -> Just LangHungarian
    "italian" -> Just LangItalian
    "japanese" -> Just LangJapanese
    "javanese" -> Just LangJavanese
    "kannada" -> Just LangKannada
    "korean" -> Just LangKorean
    "lahnda" -> Just LangLahnda
    "macedonian" -> Just LangMacedonian
    "mandarin" -> Just LangMandarin
    "marathi" -> Just LangMarathi
    "norwegian" -> Just LangNorwegian
    "polish" -> Just LangPolish
    "portuguese" -> Just LangPortuguese
    "romanian" -> Just LangRomanian
    "russian" -> Just LangRussian
    "spanish" -> Just LangSpanish
    "swedish" -> Just LangSwedish
    "tamil" -> Just LangTamil
    "telegu" -> Just LangTelegu
    "turkish" -> Just LangTurkish
    "urdu" -> Just LangUrdu
    "vietnamese" -> Just LangVietnamese
    _ -> Nothing

standardLanguageNative :: StandardLanguage -> Text
standardLanguageNative r =
  case r of
    LangEnglish -> "English"
    LangArabic -> "عربى"
    LangBengali -> "বাংলা"
    LangCantonese -> "粵語"
    LangDutch -> "Nederlands"
    LangFrench -> "Francais"
    LangGerman -> "Deutsch"
    LangHindi -> "हिंदी"
    LangHebrew -> "עברית"
    LangJapanese -> "日本語"
    LangJavanese -> "wong jawi"
    LangKannada -> "ಕನ್ನಡ"
    LangKorean -> "한국어"
    LangLahnda -> "Lahnda" -- TODO
    LangMandarin -> "普通话"
    LangMarathi -> "Marathi" -- TODO
    LangPortuguese -> "Portuguesa"
    LangRussian -> "русский"
    LangSpanish -> "Español"
    LangTamil -> "தமிழ்"
    LangTelegu -> "Telegu"
    LangTurkish -> "Türkçe"
    LangUrdu -> "اردو"
    LangVietnamese -> "Tiếng Việt"
    LangPolish -> "Polskie"
    LangDanish -> "Dansk"
    LangItalian -> "Italiano"
    LangMacedonian -> "македонски"
    LangGreek -> "Ελληνικά"
    LangFinnish -> "Suomi"
    LangSwedish -> "Svenska"
    LangNorwegian -> "Norska"
    LangRomanian -> "Română"
    LangHungarian -> "Magyar"

instance StandardField StandardLanguage where
  standardToText = standardLanguageText
  standardFromText = parseStandardLanguage

type Language = StandardOrRaw StandardLanguage

languageText :: Language -> Text
languageText = standardOrRawText standardLanguageText

parseLanguage :: Text -> Language
parseLanguage = parseStandardOrRaw parseStandardLanguage
