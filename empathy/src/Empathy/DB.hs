{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Empathy.DB where

import Data.ByteString (ByteString)
import Data.Data
import Data.Text (Text)
import Database.Persist.Sql
import Database.Persist.TH
import Empathy.HashedPassword
import Empathy.Language
import GHC.Generics (Generic)

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|

User
    email Text
    password HashedPassword

    UniqueUserEmail email

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable


Listener
    user UserId
    available Bool
    name Text
    country Text Maybe default=NULL
    email Text Maybe default=NULL
    phone Text Maybe default=NULL
    internationalCalls Bool default=0
    whatsapp Text Maybe default=NULL
    signal Text Maybe default=NULL
    telegram Text Maybe default=NULL
    skype Text Maybe default=NULL
    ghangouts Text Maybe default=NULL
    volunteer Bool default=0
    nvcTrainer Bool default=0
    professional Text Maybe default=NULL
    photo PhotoId Maybe default=NULL

    UniqueListenerUser user

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable


ListenerLanguage
    listener ListenerId
    language StandardLanguage

    UniqueListenerLanguage listener language

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable


Photo
    listener ListenerId
    contentType ByteString
    data ByteString

    UniqueListenerPhoto listener

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable

|]
