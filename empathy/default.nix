{ mkDerivation, base, bcrypt, blaze-html, bytestring, cassava
, containers, data-default, deepseq, esqueleto, http-client
, http-client-tls, JuicyPixels, JuicyPixels-extra, lib, microlens
, monad-logger, path-pieces, persistent, persistent-sqlite
, persistent-template, pretty-show, random-shuffle, shakespeare
, template-haskell, text, yesod, yesod-auth, yesod-form
, yesod-static, zip-archive
}:
mkDerivation {
  pname = "empathy";
  version = "0.0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    base bcrypt blaze-html bytestring cassava containers data-default
    deepseq esqueleto http-client http-client-tls JuicyPixels
    JuicyPixels-extra microlens monad-logger path-pieces persistent
    persistent-sqlite persistent-template pretty-show random-shuffle
    shakespeare template-haskell text yesod yesod-auth yesod-form
    yesod-static zip-archive
  ];
  executableHaskellDepends = [ base ];
  homepage = "https://github.com/NorfairKing/empathy#readme";
  license = lib.licenses.unfree;
  hydraPlatforms = lib.platforms.none;
  mainProgram = "empathy";
}
