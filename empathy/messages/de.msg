# Home Page

SearchListener: Ein/e Zuhörer/in suchen
SpokenLanguages number: Unsere Zuhörer/innen sprechen mindestens #{show number} Sprachen. Hier können Sie Ihre suchen...
Donate: Spenden Sie hier

HowItWorksTitle: Wie es funktioniert
HowItWorksContents: Dieses Angebot vernetzt Sie - jemand die/der im medizinischen und Pflegebereich, in Spitälern, Apotheken,...  während dieser COVID-19-Notlage tätig ist - mit eine/r professionellen empathischen Zuhörer/in. Finden Sie heraus, wer online ist und treten Sie in Verbindung mit jemandem, der/die am anderen Ende der Leitung zuhört. Wir möchten, dass Sie einen Ort haben, wo Sie all das mitteilen können, was Sie erleben, um gehört zu werden und unterstützt in diesen beispiellosen Zeiten. Es kann auch nur ein 10 minütiges Gespräch sein. Dieses Angebot ist kostenlos (es können Telefon- oder Internetgebühren anfallen gemäß Ihrem Vertrag mit Ihrem Anbieter)


ReadMore: Mehr Information

Sponsors: Sponsoren
NoSponsorsYet: Noch keine Sponsoren!
ContactUsInvitation: Wenn Sie oder Ihr Unternehmen diese Initiative finanziell unterstützen möchten, kontaktieren Sie uns bitte.
ContactUs: Kontakt

WEMOnline: WEM Online
Acknowledgments: Danke
WebsiteBy: Website by CS Syd
LogoBy: Logo by Studio Mira
VisualsBy: Visuals von Visfera
InCollaborationWith: In Zusammenarbeit mit BlaBla
PrivacyBy: Datenschutzdienste von DOP4You
DisclaimerTitle: Disclaimer
DisclaimerContents: Diese Website wurde in großer Eile zusammengestellt. Wir bitten um Nachsicht.

# Privacy / Cookies
PrivacyPolicyTitle: Datenschutzerklärung
CookiePolicyTitle: Cookies

# Search Page

ListenersAvailableTotal available@Int total@Int: #{show available} Erreichbare Zuhörer/innen und #{show total} Zuhörer/innen gesamt
Search: Suchen


HoldCtrl: Halten Sie Ctrl gedrückt (Cmd für Mac) um mehrere Sprachen auszuwählen.
ForgiveTranslations: Wir bitten um Nachsicht für alle fehlerhaften Übersetzungen für Namen der Sprachen.


MatchingListeners: Gefundene Zuhörer/innen
ListenersSearchLangAvail langlist langmatch@Int langavailable@Int: Sie haben nach Zuhörer/innen in folgender Sprache gesucht  #{langlist}: #{show langmatch} sind jetzt gerade bereit, einen Anruf entgegenzunehmen (von insgesamt #{show langavailable} Zuhörer/innen, die diese Sprache sprechen).

ListenersMatchAvailableTotal available@Int total@Int: Derzeit stehen auf WEM insgesamt #{show available} Zuhörer/innen für Anrufe zur Verfügung (von insgesamt #{show total} registrierten Zuhörer/innen).

SelfResponsibility: Dieses Angebot beruht auf Selbstverantwortung und im Geist von Geben und Nehmen. 
VolunteeringNotice: Zuhörer/innen bieten ihre Zeit und ihre Verfügbarkeit ehrenamtlich, sie erhalten dafür keinen finanziellen Ausgleich. 
InternationalCallsNotice: Bevor Sie eine Mobiltelefonnummer in einem anderen Land anrufen, berücksichtigen die Kosten für Auslandsgespräche und ob die/der Zuhörer/in internationale Anrufe annimmt. 

Professional title@Text: Beruf: #{title}
NVCTrainer: GFK Trainer/in
Country name@Text: Land: #{name}
Languages: Sprachen
EmailAddress: Email
PhoneNumber: Telefonnummer
InternationalCalls: Internationale Anrufe

# Authentication

LoginFormTitle: Login
EmailAdress: Email-Adresse
Passphrase: Passwort
LoginFormAction: Einloggen
Or: Oder

NoUserFoundWithEmailAndPassword email@Text: Es existiert kein Benutzerkonto mit dieser Email-Adresse #{email} und dem angegebenen Passwort.
PasswordMismatch: Die Passwörter stimmen nicht überein
EmailAlreadyExistsError email@Text: Es existiert bereits ein Benutzerkonto mit dieser Email-Adresse "#{email}"
HashFailure: Der Server konnte dieses Passwort nicht hashen.

RegistrationFormTitle: Registrieren
RepeatPassphrase: Passwort wiederholen
RegistrationFormAction: Registrieren
RegistrationHelp: Bitte notiere dein Passwort bevor du die Registrierung sendest. Nach derzeitigem Stand kannst du dich nicht einloggen oder dein Passwort ändern, wenn du es vergisst.

# Listener
ListenerResponsibilityNote: Bitte beachte, dass du für deine Verfügbarkeit verantwortlich bist. Wenn du dich als “verfügbar” markierst, wird dein Profil sichtbar bei den Suchresultaten für verfügbare Zuhörer/innen.
Available: Du bist derzeit verfügbar.
Unavailable: Du bist derzeit nicht verfügbar.
MarkAvailable: Mich als verfügbar markieren. 
MarkUnavailable: Mich als nicht verfügbar markieren.
FileTypeNotAllowed type@Text: Dateiformat nicht erlaubt: #{type}
AllowedFileTypes types@Text: Erlaubte Dateiformate: #{types}
ImageProcessingProblem error@Text: Problem bei der Verarbeitung des Bildes: #{error}
AtLeastOneVoiceOptionError: Wir möchten die Unterstützung in Form von Gesprächen anbieten. Deshalb fülle bitte mindestens eines der folgenden Felder in deinem Profil aus: Telefonnummer, Whatsapp, Signal, Telegram, Skype oder Google Hangouts. Bitte vervollständige dein Profil. 

ProfilePageTitle: Profil
LogOutAction: Log out
BackHome: Zurück zur Startseite


Suggestions1: Einfache Vorschläge, wie du ein Gespräch beginnen kannst (Es ist hilfreich, zu Beginn klar zu sagen, wieviel Zeit du hast und auch, dass du auf die Zeit achtest, sodass der/die Anrufer/in darauf nicht zu achten braucht.)
Suggestions2: “Hallo, mein Name ist ….. Ich habe 45 Minuten für dieses Gespräch Zeit und ich werde auf die Zeit achten. Möchten Sie mir erzählen, was Ihnen gerade durch den Kopf geht?” Oder: “Was bewegt Sie gerade?” Oder: “Wie geht es Ihnen?” Oder: “Was haben Sie auf dem Herzen?”

GoogleFormInvitation: Bitte fülle dieses Google-Form, jedes Mal, wenn du einer Person zugehört hast aus. So können wir ein klares Bild davon bekommen, wie viele Personen unser Angebot nützen.  

ProfileCardTitle: Profilkarte
ProfileCardPreview: So werden deine Informationen öffentlich in den Suchergebnissen angezeigt, wenn du verfügbar bist um Anrufe entgegenzunehmen: 

UpdateProfileTitle: Profil aktualisieren
ProfileNotVisibleYet: Dein Profil ist noch nicht sichtbar.
InfoChangableNotice: Alle untenstehenden Informationen können später geändert werden.

SetupProfileTitle: Profil einrichten

NameLabel: Voller Name oder Nickname
CountryLabel: Land
ProfessionalListenerLabel: Ich bin professionelle Zuhörer/in
ProfessionalListenerHelp: Beinhaltet Psychiater/innen, Psycholog/innen, Psychotherapeut/innen, etc. Bitte gib hier deine abgeschlossenen Ausbildungen und Qualifikationen an.
NVCTrainerLabel: Ich bin GFK-Trainer/in.
PictureLabel: Bild
PictureUploadLabel: Bild hochladen
ContactLabel: Wie können wir dich kontaktieren?
ContactHelp: Diese Information wird öffentlich angezeigt. Bitte gib an, was am angenehmsten für dich ist. Wir möchten das Angebot in Form von Gesprächen machen. Deshalb bitten wir dich, zumindest eines der folgenden Felder in deinem Profil auszufüllen: Telefonnummer, Whatsapp, Signal, Telegram, Skype oder Google Hangouts. 
EmailLabel: Email-Adresse
PhoneCallLabel: Telefonnummer
PhoneNumberPlaceholder: Telefonnummer
InternationalPhonecallsLabel: Ich nehme auch internationale Anrufe an.
CountryCodeHelp: Gib die Vorwahl deines Landes ein.
AvailableLabel: Ich bin zur Zeit bereit, kontaktiert zu werden.
AvailableHelp: Du wirst in der Liste der Suchresultate für Zuhörer/innen nicht angezeigt, wenn du dich nicht als verfügbar markiert hast.

ChecklistLabel: Ich verstehe und bin damit einverstanden, dass ich meine Zeit als Zuhörer/in ehrenamtlich zur Verfügung stelle und ich dafür kein Geld bezahlt bekomme, weder von den Anrufer/innen, denen ich Unterstützung anbiete, noch von den Organisationen, die mit dieser Website in Verbindung stehen. Ich bin damit einverstanden, dass meine Kontaktinformationen auf dieser Website öffentlich sichtbar sein werden. Mir ist bewusst, dass dieses Angebot auf der Selbstverantwortung der Benutzer/innen beruht und auch wenn ich angegeben habe, keine internationalen Anrufe anzunehmen, kann ich evtl. welche erhalten und es können mir Kosten entstehen, wenn ich diese annehme  - je nach den Bedingungen in meinem Telefonvertrag.  

SaveProfileSettings: Profil speichern
BackToListener: Zurück zur Zuhörer/innen-Seite

## New messages below here
#

DeleteAccountAction: Account löschen
AccountDeletionDoubleConfirmation: Sind Sie sicher, dass Sie Ihren Account löschen möchten? Das kann nicht rückgängig gemacht werden
DownloadDataAction: Meine Daten downloaden
PressKit: Pressemappe


# Cookie policy
CookiesFor: Verwendung von cookies auf der Website erstellt für:
WEMInitiated: WEM ist ein Projekt, initiert von Dr. Luc Peetermans (B), organisiert und koordiniert on der Blabla-Foundation VZW (B) in Kooperation mit einerm internationalen Team von Trainer/innen für Gewaltfreie Kommunikation (GFK).
WhatAreCookies: CCookies sind kleine Textdateien, die auf ihrem Computer von Websites, die Sie besuchen, platziert werden. Sie werden oft verwendet, damit Websites funktionieren oder besser funktionieren, oder auch um den Betreiber/innen der Website Informationen zu geben. Die untenstehende Aufstellung erklärt, welche Cookies wir verwenden und warum. 
IfListener: Für Zuhörer/innen:
IfListenerThen: Wir speichern nur die Cookies aus der Login-Session, damit wir Sie wiedererkennen, wenn Sie die Benutzerinformationen einmal löschen, werden auch auch diese Cookies gelöscht.
IfVisitor: Für Besucher/innen der Website:
IfVisitorThen: Wir bewahren die Cookies von Google Analytics auf und verwenden sie für Berichte und Statistiken. Sie können gelöscht werden, indem Sie ihren eigenen Browserverlauf löschen. 
IfQuestions: Wenn Sie weitere Fragen zu dem Minimum an Cookies haben, das wir sammeln, senden Sie bitte einfach ein email an: privacy@blabla-blabla.be.

# Privacy policy

HostedBy: Gehostet von:
HostedByAddress: Logistik & Koordination sind zentralisiert bei Blabla Foundation VZW,Oude Vorstseweg 25, 2430 Eindhout (Laakdal) - Belgien

QuestionsAboutPrivacyQuestion: Haben Sie Fragen zum Schutz Ihrer Daten?
QuestionsAboutPrivacyAnswer1: Wir, die Blabla Foundation VZW messen dem Schutz Ihrer Privatsphäre und Ihrer persönlichen Daten größte Bedeutung zu. Wir möchten dieses Vertrauen bewahren, und daher alles, was Sie uns an Daten anvertrauen auf korrekte und bestmögliche Art schützen. Wir behandeln ihre persönlichen Daten sehr achtsam. 
QuestionsAboutPrivacyAnswer2: Blabla Foundation VZW unternimmt alle Anstrengungen, die gültige Rechtslage und Vorschriften einzuhalten, einschließlich die General Data Protection Regulation (AVG / GDPR).
QuestionsAboutPrivacyAnswer3: Wenn Sie Fragen haben, nachdem Sie diese Datenschutzerklärung gelesen haben, wie wir Ihre persönlichen Daten verarbeiten oder wie Sie Ihre Rechte ausüben können, freuen wir uns, Ihnen dabei behilflich zu sein und Ihre Fragen zu beantworten.
PersonalDataQuestion: Welche Ihrer persönlichen Daten verarbeiten wir?
PersonalDataAnswer1: Wir, die Blabla Foundation VZW, verarbeiten Ihre persönlichen Daten nur, nachdem Sie sich angemeldet haben und die Einladung ausgefüllt haben, die wir an Sie gesendet haben. Nachdem Sie das Formular ausgefüllt haben, werden Sie als Zuhörer/in gelistet. Sie haben immer die Möglichkeit, die Cookies aus der Login-Session zu aktivieren, Sie haben Wahl, ob wir sie speichern sollen oder nicht (mehr Informationen finden Sie in der Cookie Policy).
PersonalDataAnswer2: Wir möchten die Benutzeroberfläche für Sie und die Benutzer/innen so einfach wie möglich halten. Als Zuhörer/innen stellen wir Ihnen zur freien Wahl, welche Daten Sie uns bekanntgeben möchten, sodass Sie einfach gefunden werden können und es klar für die Suchenden ist:
Biography: Biografie
IPAddress: IP Adresse
IPAddressClarification: Ihre IP-Adresse ist Teil der Internetprotokolle, so funktioniert das Internet, wir speichern sie nirgendwo und verfolgen sie auch in keiner Weise nach. 
PhotoUploadCare: Bitte überlegen Sie gut, ob Sie ein Foto von sich hochladen. Aufgrund des hochgeladenen Fotos kann jemand rassische Zuschreibungen machen oder Rückschlüsse auf Ihren ethnischen Hintergrund, Ihre religiösen oder philosophischen Hintergründe ziehen. Wenn Sie trotzdem beschließen, das Foto hochzuladen, seien Sie sich bewusst, dass das eine sensible Information ist. 
DataProcessQuestion: Warum verarbeiten wir diese Daten?
DataProcessAnswer1: Wir, die Blabla Foundation VZW, verarbeiten Ihre Daten mit dem Ziel, Sie gut unterstützen und begleiten zu können. Wir verarbeiten nur die Daten, die Sie uns mit Ihrer Zustimmung zukommen haben lassen.
DataProcessAnswer2: Sie können jederzeit Ihre Daten verändern, korrigieren, ergänzen oder auch löschen. 
DataProcessAnswer3: Ihre Daten werden nicht länger als nötig aufbewahrt.
PersonalDataUseQuestion: Wie verwenden wir Ihre persönlichen Daten?
PersonalDataUseAnswer1: Unsere Trainer/innen (GFK) sehen nur die Daten aus ihrer Region, das kann sowohl geografisch als auch in Bezug auf ihre Erfahrungen sein, die Sie uns angegeben haben. Sie werden Sie auf diese Weise kontaktieren, die Sie auf dem Formular ausgewählt haben. 
PersonalDataUseAnswer2: Daten, die keine persönlichen Daten beinhalten, werden für Statistiken und Berichte verwendet. Diese Daten können niemals mit Ihrer Person in Verbindung gebracht werden. 
PersonalDataUseAnswer3: Blabla Foundation VZW wird die von Ihnen angegebenen persönlichen Daten nie an Dritte weitergeben, außer das wird per Gesetz oder durch sehr außergewöhnliche Notfälle nötig.
PersonalDataUseAnswer4: Die Verarbeitung Ihrer persönlichen Daten beinhaltet kein Profiling und Sie werden auch nicht für automatisierte Entscheidungen der Blabla Foundation VZW verwendet.
RightsTitle: Ihre Rechte:
Rights1: Sie können jederzeit eine Aufstellung der Daten, die Sie uns gegeben haben, verlangen oder Sie können um Veränderung bitten, wenn Sie das selbst nicht tun können. Oder Sie können uns bitten, die von Ihnen angegebenen Daten zu löschen.
Rights2: Sie können auch Einwände dagegen erheben, dass wir die von Ihnen angegebenen Daten verarbeiten, wenn Sie nicht mit unserer Vorgangsweise zur Verwendung Ihrer Daten für die für uns notwendigen Zwecke einverstanden sind.
Rights3: Sie können das tun, indem Sie eine email schicken an:privacy@blabla-blabla.be
Rights4: Wir sind gerne für Sie da, wenn Sie Beschwerden zu der Verarbeitung Ihrer persönlichen Daten haben. Wenn wir uns trotz allem nicht einigen können, haben Sie das Recht, eine BEschwerde an die Data Protection Authority, zu richten, dieser Link führt direkt auf die Website der Europäischen Kommission, Sie können dort einfach Ihre bevorzugte Sprache wählen in der Ecke, rechts oben auf der Seite. 
DataProtectionAuthority: Data Protection Authority https://ec.europa.eu/assets/sg/report-a-breach/complaints_de/index.html
Rights5: Wir arbeiten mit hoch motivierten Freiwilligen zusammen, unser Ziel ist, für Menschen da zu sein, die eine/n Zuhörer/in brauchen, was unserer Einschätzung nach jetzt gerade besondere Dringlichkeit hat. Unsere Datenschutzerklärung wird daher regelmäßig aktualisiert. Wir arbeiten alle 24/7, um für Sie da zu sein. Die Optimierung unserer ehrenamtlichen Arbeit ist uns sehr wichtig!


# FAQ

IsThisForMeTitle: Ist das für mich?
IsThisForMeSubTitle: Vielleicht machen Sie sich Gedanken über die Möglichkeit eine/n Zuhörer/in anzurufen... 

FaqIdentityQuestion: Muss ich angeben, wer ich bin oder von wo aus ich anrufe?
FaqIdentityAnswer: Wir brauchen keine Information über Sie, wir fragen nur nach der Region, von der aus Sie anrufen, damit wir Rückmeldung zur Sichtbarkeit und Nutzung unseres Angebots bekommen. 

FaqCostsQuestion: Habe ich für die Nutzung des Angebots Kosten?
FaqCostsAnswer: Nein, das Angebot ist kostenlos - die Zuhörer/innen arbeiten ehrenamtlich. Bevor Sie eine Telefonnummer in einem anderen Land anwählen, ziehen Sie bitte in Betracht, ob für Sie dadurch Kosten anfallen (je nach Vertrag mit Ihrer Telefongesellschaft bzw. Ihrem Internetprovider) und auch ob die/der gewählte Zuhörer/in internationale Anrufe annimmt. 

FaqWorkingsQuestion: Wie läuft das ab? Wie fange ich an?
FaqWorkingsAnswer: Unsere Zuhörer/innen wissen, wie man anfangen kann. Sie sind willkommen, wie auch immer Sie in den Anruf kommen.

FaqProblemsQuestion: Wie kann ich wissen, ob meine Probleme wichtig genug sind, um bei WEM anzurufen?
FaqProblemsAnswer: Alles, was Ihren Arbeitsalltag in dieser Covid19-Pandemie betrifft, ist willkommen gehört zu werden, wenn Sie unsere Zuhörer/innen anrufen. Vielleicht sind Sie müde, überfordert, haben die Nase voll von vielen Arbeitsstunden und möchten das gerne jemandem erzählen, bevor Sie nach Hause gehen? Vielleicht ist etwas Gravierendes passiert in Ihrer Arbeit, heute oder vor Wochen, das sie noch immer belastet? Vielleicht haben Sie den Eindruck, dass Ihr Stress bereits Ihre Familie belastet und Sie über manche Arbeitsthemen zu Hause nicht sprechen können? Was auch immer Sie auf dem Herzen haben und mit Ihrer Arbeit im Gesundheitswesen zu tun hat ist willkommen, unseren Zuhörer/innen hören Ihnen gerne zu - ob ihre Themen groß oder klein scheinen. 

FaqEmpathyQuestion: Was ist Empathie?
FaqEmpathyAnswer1: Unsere Zuhörer/innen sind geschult in einer tiefgehenden Form einfühlsamen und verständnisvollen Zuhörens. Wir nennen sie Empathie und sie beruht auf einer bedingungslos positiven Sichtweise, die einen wertungsfreien Raum schafft; einen Raum, in dem eine andere Person präsent ist mit dem, was Sie gerade bewegt. Alle Zuhörer/innen sind dazu ausgebildet, Raum für Sie zu halten, in dem Sie einfach Sie selbst sein können.Viele von uns haben viele Jahre Erfahrung in Schulen, Gefängnissen, Krankenhäusern, mit unseren eigenen Familien und manche von uns auch mit Einzelsitzungen. Wir sehen viele Vorteile von Empathie für Menschen im Gesundheitswesen:
FaqEmpathyAnswerList1: Stressreduktion
FaqEmpathyAnswerList2: Burn-Out-Prävention
FaqEmpathyAnswerList3: Selbstregulierung, Klarheit
FaqEmpathyAnswerList4: Selbstfürsorge
FaqEmpathyAnswerList5: Orientierung für hilfreiches Verhalten in Bezug auf sich selbst und Andere
FaqEmpathyAnswer2: So können wir damit fortfahren, an einer Welt zu arbeiten, wo für alle genug von dem da ist, was sie brauchen.

FaqComfortQuestion: Mir ist es unangenehm, dass ich HIlfe brauche…
FaqComfortAnswer: Vielleicht ist es schwierig, darum zu bitten, weil Sie das noch nie getan haben? Oder ist es schwierig, weil es so aussieht, als würden es alle anderen auch ohne Hilfe schaffen? Jemandem zu erzählen, wie es Ihnen geht, kann ein großer Schritt sein und wir ermutigen Sie, Hilfe anzunehmen, auch wenn es für Sie neu oder ungewohnt ist. Sie und Ihre Bedürfnisse sind wertvoll, so wertvoll wie die aller anderen. Sie sind willkommen mit Ihrem Anruf. 

FaqBotheringQuestion: Falle ich der/dem Zuhörer/in zur Last?
FaqBotheringAnswer: Unsere Zuhörer/innen bieten ihre Präsenz an, weil sie gerne beitragen möchten. Wir trauen jedem und jeder von ihnen zu, ihr Zuhören anzubieten, weil sie wissen, dass sie auch davon profitieren. Einer von ihnen hat uns sogar folgendes berichtet: “Ich bin so froh und sogar irgendwie erleichtert, Teil des WEM-Projekts zu sein. Ich bin jeden Tag allein in meinem Haus und ich kann mir gar nicht vorstellen, wie es für Sie im Zentrum der Corona-Krise ist. Ich möchte gerne in dieser Krise nützlich sein, indem ich als Zuhörer für andere den Raum halte. Das gibt mir Sinn und eine Aufgabe in diesen Tagen… Ich möchte, dass Sie wissen, dass Sie meine Tage verschönern, wenn Sie das WEM-Service nutzen. Ich bin hier und ich möchte gerne für andere da sein, nicht um Ihnen zu “helfen”, sondern um Ihnen mit meiner Präsenz beizustehen, von Herz zu Herz, von Mensch zu Mensch.”

FaqStrangerQuestion: Es fällt mir schwer mir vorzustellen, mit einem Fremden zu reden...
FaqStrangerAnswer: Vielleicht ist es ungewohnt, mit fremden Menschen zu reden und ihre Freundlichkeit und Großzügigkeit anzunehmen? Und wir möchten die Dinge anders machen. Eines unserer Ziele ist, die Menschen näher zusammenzubringen mit Empathie. Unsere Antwort auf die Krise ist zu zeigen, dass es möglich ist, füreinander da zu sein, vielleicht so, wir es zuvor noch nie getan haben. Wenn wir in Ihr Gesundheitszentrum, Krankenhaus oder Ihre Apotheke kommen, wissen wir, dass wir von Ihnen umsorgt werden. Nun möchten wir Sie auf dieselbe Art willkommen heißen. Am Ende des Anrufs sind wir einander näher und keine Fremden mehr. 

FaqTalkingQuestion: Wie kann Reden allein helfen?
FaqTalkingAnswer: Vielleicht machen Sie sich Sorgen, dass ein Anruf Zeitverschwendung ist? Oder dass Sie sich so in Ihren Gefühlen verlieren, dass Sie nicht mehr Ihre Arbeit tun können? Unsere Zuhörer/innen sind für Empathie ausgebildet. Viele Menschen berichten, dass diese Art gehört zu werden ihre innere Kapazität erhöht, weiterzumachen und zu verarbeiten, was ihnen passiert ist. Wir möchten so für Sie da sein, dass Sie ihre inneren Ressourcen finden, mit ihrer Arbeit zurechtzukommen. 

FaqQuote: “Immer wieder wachsen Menschen über die paralysierenden Auswirkungen psychischen Schmerzes hinaus, wenn sie ausreichend in Kontakt sind mit jemandem, der sie empathisch hören kann.” - Dr. Marshall Rosenberg  

FaqThanks: Danke, dass Sie so weit gelesen haben. Das sind einige Fragen, die uns in Fürsorge für Sie in den Sinn gekommen sind. Wenn Sie andere Überlegungen oder Vorbehalte haben, lassen Sie uns diese bitte wissen, damit wir daraus lernen können.  Schreiben Sie uns eine Email.

# About US
AboutUs: Über uns
WhatWeDoTitle: Was wir tun

WhatWeDoPar1: Worldwide Empathy for Medics (WEM)  www.wem.icu  vernetzt medizinisches Personal und alle, die im Gesundheitswesen arbeiten, mit empathischen Zuhörer/innen, mit der Idee, für diejenigen zu sorgen, die für andere sorgen. 

WhatWeDoPar2: Angesichts der weltweiten Covid-19- Pandemie haben wir uns zusammengetan, weil wir glauben, dass gehört zu werden eine stärkende Erfahrung sein kann. Deshalb möchten wir das, was wir gut können, jenen anbieten, die in Krankenhäusern, Pflegeheimen und medizinischen Einrichtungen aller Art arbeiten. Unsere Vision ist, dass medizinisches Personal weltweit empathische Zuhörer/innen finden kann, 24 Stunden täglich. Und wir hoffen, dass Sie das dabei unterstützt, ihre Arbeit fortzusetzen und physisch und psychisch gesund zu bleiben in diesen schweren Zeiten. 

WhatWeDoPar3: Empathie bedeutet, für uns, jemand anderem vollständig zuzuhören, ohne Ratschläge zu geben oder auf eine Lösung hinzusteuern. Deshalb bietet WEM keine Therapie oder Beratung an, sondern Menschen, die mit ganzem Herzen zuhören, egal was gerade da ist und ein Ohr braucht, um gehört zu werden, von professionellen bis zu privaten Belastungen und Sorgen. 

WhatWeDoPar4: Dieses Angebot ist kostenlos und kann so oft und so lange in Anspruch genommen werden, wie Sie möchten. 

WhoWeAreTitle: Wer wir sind

WhoWeArePar1: WEM ist ein Projekt, initiiert von Dr. Luc Peetermans, organisiert und koordiniert von der Blabla-Foundation VZW in Zusammenarbeit mit einem internationalen Team.

BehindTitle: Hinter den Kulissen 

BehindPar1: Unser Koordinationsteam ist ein internationales Netzwerk aus vielen Berufen, es besteht aus zertifizierten Trainer/innen für Gewaltfreie Kommunikation (GFK), Psycholog/innen, Prozessbegleiter/innen, Coaches, Lehrer/innen, Therapeut/innen, Grafikdesigner/innen und Administration. Unsere Entscheidung zusammenzuarbeiten war inspiriert von unseren geteilten Werten, u.a. Gegenseitigkeit, Aufrichtigkeit, Freiwilligkeit und Leadership, Lernen und Zusammenleben mit Empathie. Wir haben schon in früheren Projekten zusammengearbeitet, wie z.B. Life-Enriching Education Lab, Visfera-Material und Trainingsprogrammen, wie Who is training who? un NVC in Lightness and Depth. Mehr über Zertifizierung in GFK finden Sie hier.

OnThePhoneTitle: Am Telefon 

OnThePhonePar1: Unsere Zuhörer/innen sind Freiwillige aus der ganzen Welt. Sie sind zertifizierte Trainer/innen für Gewaltfreie Kommunikation oder wurden uns persönlich empfohlen. So sorgen wir u.a. für die Qualität und Zuverlässigkeit unseres Angebots. 

WemTeamTitle: Das ist unser WEM-Koordinations-Team:

