# Home Page

SearchListener: Rechercher un(e) écoutant(e)
SpokenLanguages number: Nos écoutants parlent au moins #{show number} langues. Vérifiez la vôtre...
Donate: Faire un don

HowItWorksTitle: Fonctionnement du site
HowItWorksContents: Ce service  met en contact, quiconque, impliqué dans le secteur médical et de la santé, dans les hôpitaux, cliniques, pharmacies... avec un(e) écoutant(e) empathique professionnel(le) pendant cette urgence COVID-19 - Découvrez qui est en ligne et connectez-vous avec quelqu’un prêt à écouter à l’autre bout du fil. Nous voulons que vous ayez un endroit pour partager tout ce que vous vivez, pour être entendu(e) et soutenu(e) en cette période sans précédent. L’appel peut ne durer que 10 min. Ce service est gratuit (des frais de téléphone ou d’Internet peuvent s’appliquer selon votre contrat avec votre fournisseur).
ReadMore: Pour en savoir plus

Sponsors: Sponsors
NoSponsorsYet: Pas encore de sponsors!
ContactUsInvitation: Si vous ou votre entreprise souhaitez parrainer cet effort, s’il vous plaît, contactez-nous.
ContactUs: Pour nous contacter

WEMOnline: WEM Online
Acknowledgments: Remerciements
WebsiteBy: Site internet by CS Syd
LogoBy: Logo by Studio Mira
VisualsBy: Visuels par Visfera
InCollaborationWith: En collaboration avec BlaBla
PrivacyBy: Services de confidentialité par DOP4You
DisclaimerTitle: Dégagement de responsabilité
DisclaimerContents: Ce site internet a été créé en très peu de temps. Merci de votre compréhension.

# Privacy / Cookies
PrivacyPolicyTitle: Confidentialité
CookiePolicyTitle: Cookies

# Search Page

ListenersAvailableTotal available@Int total@Int: #{show available} écoutant(e)s disponible et #{show total}  total d’écoutant(e)s 
Search: Recherche


HoldCtrl: Maintenir Ctrl (Cmd on Mac) appuyé pour sélectionner différentes langues
ForgiveTranslations: Veuillez m’excuser pour toute traduction incorrecte des noms des langues.


MatchingListeners: Ecoutant(e)s trouvé(e)s
ListenersSearchLangAvail langlist langmatch@Int langavailable@Int: Vous avez cherché le nombre d’écoutants disponibles en #{langlist}: #{show langmatch} écoutants sont disponible en ce moment pour recevoir un appel, sur #{show langavailable} écoutants enregistrés qui parle cette langue.

ListenersMatchAvailableTotal available@Int total@Int: En ce moment sur WEM il y a un total de #{show available} écoutants disponibles pour recevoir des appels sur #{show total} écoutants enregistrés. 

SelfResponsibility: Ce service repose sur la responsabilité individuelle dans l’esprit de donner et recevoir. 
VolunteeringNotice: Les écoutant(e)s offrent leur temps et disponibilité en tant que bénévoles, sans recevoir de compensation financière
InternationalCallsNotice: Avant d’appeler un numéro mobile dans un autre pays, veuillez tenir compte des coûts  internationaux et si l’écoutant(e) accepte les appels internationaux

Professional title@Text: Profession: #{title}
NVCTrainer: Formateur(trice) en CNV
Country name@Text: Pays: #{name}
Languages: Langues
EmailAddress: Email
PhoneNumber: Numéro de téléphone
InternationalCalls: Appels internationaux


# Authentification

LoginFormTitle: S’identifier
EmailAdress: Adresse email
Passphrase: Mot de passe
LoginFormAction: Se connecter
Or: Ou

NoUserFoundWithEmailAndPassword email@Text: Il n’y a pas d’utilisateur enregistré avec cette adresse mail: #{email} et le mot de passe donné..
PasswordMismatch: Mot de passe erroné
EmailAlreadyExistsError email@Text: Il y a déjà un utilisateur avec cette adresse mail: "#{email}"
HashFailure: Le serveur n’a pas été en mesure d’exécuter le hachage de ce mot de passe..

RegistrationFormTitle: S’enregistrer
RepeatPassphrase: Répétez le mot de passe
RegistrationFormAction: S’enregistrer
RegistrationHelp: Veuillez noter votre mot de passe avant de soumettre votre inscription. A ce stade du service, si vous oubliez votre mot de passe, vous ne pourrez pas vous connecter ou modifier votre mot de passe.

# Listener
ListenerResponsibilityNote: Veuillez noter que votre disponibilité est sous votre responsabilité. Quand vous vous marquez comme étant disponible, votre profil sera visible publiquement dans les résultats de recherche des écoutant(e)s disponibles.
Available: Vous êtes actuellement disponible.
Unavailable: Vous êtes actuellement indisponible.
MarkAvailable: Me marquer comme disponible.
MarkUnavailable: Me marquer comme indisponible.
FileTypeNotAllowed type@Text: Format de fichier non pris en compte: #{type}
AllowedFileTypes types@Text: Format de fichier pris en compte: #{types}
ImageProcessingProblem error@Text: Problème pendant le traitement de l’image: #{error}
AtLeastOneVoiceOptionError: Nous voulons offrir un soutien sous la forme d’un « service vocal». Pour cette raison, nous vous demandons de remplir au moins un des champs suivants dans votre profil : Numéro de téléphone, Whatsapp, Signal, Telegram, Skype ou Google Hangouts. Merci de compléter votre profil.

ProfilePageTitle: Profil
LogOutAction: Se déconnecter
BackHome: Retour à la page d’accueil


Suggestions1: Quelques suggestions pour  commencer une session (il est  utile de nommer clairement de combien de temps vous disposez et d’être la personne qui gère ce temps afin que l’appelant n’assume aucune responsabilité pour cela.)
Suggestions2: « Bonjour, Je m’appelle…. Je propose que cet appel dure 45 minutes et je vais me charger de vérifier le temps écoulé. Pouvez-vous partager ce qui se passe pour vous? Ce que vous avez en tête? Ce que vous avez sur le coeur? ou «Comment vous sentez-vous? »
GoogleFormInvitation: S’il vous plaît, veuillez remplir ce formulaire Google à chaque fois que vous avez écouté un soignant. De cette façon, nous pouvons avoir une idée claire du nombre de personnes qui utilisent notre service.

ProfileCardTitle: Fiche de profil
ProfileCardPreview: Voici ce qui sera visible publiquement dans les résultats de recherche des écoutant(e)s, lorsque vous serez disponible pour recevoir des appels :

UpdateProfileTitle: Mettre à jour votre profil
ProfileNotVisibleYet: Votre profil n’est pas visible pour le moment.
InfoChangableNotice: Toutes les informations ci-dessous peuvent être changer ultérieurement.

SetupProfileTitle: Configurer votre profil

NameLabel: Prénom/Nom ou Surnom
CountryLabel: Pays
ProfessionalListenerLabel: Je suis professionnel(le) de l’écoute
ProfessionalListenerHelp: Cela comprend les psychiatres, les psychologues, les thérapeutes, etc... Merci de renseigner vos diplômes et qualifications.
NVCTrainerLabel: Je suis formateur(trice) en CNV
PictureLabel: Photo
PictureUploadLabel: Téléchargez votre photo
ContactLabel: Comment peut-on vous contacter?
ContactHelp: Cette information sera publique. Indiquez ce qui vous convient le mieux. Nous voulons offrir du soutien sous la forme d’un « service vocal ». Pour cette raison, nous vous demandons de remplir au moins un des champs suivants dans votre profil : Numéro de télephone, Whatsapp, Signal, Telegram, Skype ou Google Hangouts.
EmailLabel: Adresse mail
PhoneCallLabel: Appel téléphonique
PhoneNumberPlaceholder: Numéro de téléphone
InternationalPhonecallsLabel: J’accepte de recevoir des appels internationaux.
CountryCodeHelp: Veuillez inclure l’indicatif international .
AvailableLabel: Je suis disponible en ce moment pour recevoir des appels.
AvailableHelp: Vous ne serez pas visible dans les résultats de recherche montrant les écoutant(e)s si vous ne vous êtes marqué(e) comme disponible.
ChecklistLabel: Je comprends et j’accepte que le fait d’offrir mes services/mon temps en tant qu’écoutant(e)  est fait sur la base du bénévolat et qu’aucun paiement ne sera dû par les personnes à qui j’offrirai du soutien, ni par les organisations liées à ce site Web. Je comprends que mes coordonnées seront affichées publiquement sur ce site Web. Je suis conscient que ce service est basé sur l’auto-responsabilité des utilisateurs. Même si j’indique que je ne veux pas recevoir d’appel international, il se peut que j’en ai, entraînant des coûts  téléphoniques selon mon opérateur mobile.


SaveProfileSettings: Enregistrez les paramètres de votre profil
BackToListener: Retour sur la page écoutant(e)

## New messages below here
#

DeleteAccountAction: Supprimer votre compte
AccountDeletionDoubleConfirmation: Etes-vous certain de vouloir supprimer votre compte? Ceci est irréversible.
DownloadDataAction: Télécharger mes données
PressKit: Dossier de Presse


# Cookie policy
CookiesFor: Utilisation des cookies par le site créé pour :
WEMInitiated: WEM est un projet initié par Dr. Luc Peetermans (B), créé et coordonné par Blabla-Foundation VZW (B) en collaboration avec une équipe internationale de formateurs en Communication NonViolente (CNV).
WhatAreCookies: Les cookies sont de petits fichiers texte qui sont placés sur votre ordinateur par les sites Web que vous visitez. Ils sont largement utilisés afin de faire fonctionner les sites Web, de travailler plus efficacement, ou de fournir des informations aux propriétaires du site. Le tableau ci-dessous explique les cookies que nous utilisons et pourquoi.
IfListener: Si vous êtes un écoutant: 
IfListenerThen: Nous stockons uniquement les cookies de session de connexion, ceci pour se souvenir de vous, une fois que vous supprimez vos identifiants, ces cookies disparaîtront également.
IfVisitor: Si vous êtes un visiteur:
IfVisitorThen: Nous conservons les cookies Google Analytics, ces cookies sont utilisés pour les rapports et les statistiques. Ils peuvent être supprimés en nettoyant votre propre historique de navigateur.
IfQuestions: Si vous avez d’autres questions concernant les cookies que nous collectons, envoyez simplement un e-mail à : privacy@blabla-bla.be.

# Privacy policy

HostedBy: Hébergé par:
HostedByAddress: La logistique et la coordination sont centralisées par: Blabla Foundation VZW Oude Vorstseweg 25, 2430 Eindhout (Laakdal) - Belgium 
QuestionsAboutPrivacyQuestion: Questions au sujet de la confidentialité?
QuestionsAboutPrivacyAnswer1: Nous, Blabla Fondation VZW, accordons une grande importance à votre vie privée et à la protection de vos données personnelles. Nous voulons maintenir la confiance entre nous, c’est pourquoi nous nous engageons à protéger tout ce que vous avez placé entre nos mains de la façon la plus appropriée. Nous agissons uniquement dans votre intérêt et traitons vos données personnelles très soigneusement.
QuestionsAboutPrivacyAnswer2: Blabla Foundation VZW, s’efforce de se conformer aux lois et règlements applicables, y compris le Règlement général sur la protection des données (AVG / GDPR).
QuestionsAboutPrivacyAnswer3: Si vous avez des questions après avoir lu cette déclaration de confidentialité sur le traitement de vos données personnelles ou sur l’exercice de vos droits, nous serons ravis de vous aider et de répondre à vos questions.
PersonalDataQuestion: Quelles données personnelles collectons-nous?
PersonalDataAnswer1: Nous, Blabla Fondation VZW, collectons seulement les données personnelles après que vous vous êtes enregistré et que vous avez répondu à l’invitation que nous vous avons envoyé. Après avoir rempli le formulaire, vous devenez un(e) écoutant(e). Vous avez toujours la possibilité d’activer les cookies de session de connexion, cela dépend de vous si vous voulez que nous les conservions. (voir notre politique de cookies pour plus d’informations).
PersonalDataAnswer2: Nous avons pour but d’offrir une interface simple pour vous et les usagers. En tant qu’écoutants, nous vous laissons libre de choisir les données que vous souhaitez partager avec nous, afin que  les usagers puisse vous trouver facilement:
Biography: Biographie
IPAddress: Adresse IP
IPAddressClarification: Votre adresse IP est disponible dans le cadre des protocoles Internet, c’est de cette façon que l’Internet fonctionne, nous ne la stockons ni la sauvegardons nulle part, nous ne gardons aucune information de suivi et repérage.
PhotoUploadCare: Veuillez bien réfléchir avant de télécharger une photo, à partir de votre photo, votre origine ethnique, vos croyances religieuses ou philosophiques... peuvent devenir visibles.Si vous décidez cependant de procéder au téléchargement de votre photo, ayez conscience que c’est un élément sensible.
DataProcessQuestion: Pourquoi collections-nous ces données?
DataProcessAnswer1: Nous, Blabla Fondation VZW, traitons vos données dans le but de vous apporter un soutien et une assistance de qualité. Nous traitons uniquement les données qui nous sont fournies avec votre consentement.
DataProcessAnswer2: Vous avez le libre choix d’ajuster, d’adapter ou même de supprimer vos données.
DataProcessAnswer3: Vos données ne sont donc pas conservées plus longtemps que nécessaire.
PersonalDataUseQuestion: Comment utilisons-nous vos données personnelles?
PersonalDataUseAnswer1: Nos formateurs (CNV) ne voient que les données, que vous nous avez confié, qui rentrent dans leur champ d’action, ce qui peut être géographique ou/et dans leur domaine d’expérience. 
PersonalDataUseAnswer2: Les données, celles qui ne sont pas personnelles sont utilisées pour les statistiques, les enquêtes et les rapports. Ces données ne peuvent jamais être liées à vous personnellement.
PersonalDataUseAnswer3: Blabla Foundation VZW, ne transmettra jamais les données personnelles que vous fournissez à des tiers, sauf si cela est requis par la loi ou lors de situations d’urgence très exceptionnelles.
PersonalDataUseAnswer4: Le traitement de vos données personnelles n’inclut pas de profilage et vous ne serez pas non plus soumis à des décisions automatisées de  Blabla Fondation VZW.
RightsTitle: Vos droits:
Rights1: Vous pouvez demander l’accès aux données que vous nous avez fournies à tout moment, ou vous pouvez les faire ajuster si vous n’êtes pas en mesure de le faire vous-même.  Vous pouvez aussi nous demander de supprimer les données fournies.
Rights2: Vous pouvez également vous opposer à l’utilisation des données fournies si vous n’êtes pas d’accord avec  les raisons pour lesquelles nous avons besoin de les utiliser.
Rights3: Vous pouvez le faire en envoyant un courriel à : privacy@blabla-blabla.be
Rights4: Nous sommes prêts à vous aider si vous avez des réclamations concernant le traitement de vos données personnelle. Si vous ne parvenez pas à un accord avec nous, vous avez le droit de déposer une réclamation  auprès de l’autorité de protection des données (ce lien vous redirige vers le site de la Commission européenne, vous pouvez facilement changer votre langue préférée dans le coin supérieur droit de la page).
DataProtectionAuthority: ce lien https://ec.europa.eu/assets/sg/report-a-breach/complaints_fr/index.html
Rights5: Nous travaillons aujourd’hui avec des bénévoles très motivés. Nos objectifs sont d’être là pour les personnes qui ont besoin d’écoute et de soutien, compte tenu de l’urgence que nous jugeons nécessaire aujourd’hui, Notre déclaration de confidentialité sera donc adaptée régulièrement. Nous travaillons tous 24/7 pour vous apporter une offre de qualité. L’optimisation de nos services bénévoles est un plus!


# FAQ

IsThisForMeTitle: Est-ce pour moi?
IsThisForMeSubTitle: Nous avons à l’esprit que vous vous posez peut-être des questions à propos des appels...

FaqIdentityQuestion: Dois-je dire qui je suis ou d’où j’appelle?
FaqIdentityAnswer: Aucune information de votre part n’est requise, nous vous demandons d’où vous appelez dans le but d’avoir une idée de la visibilité et l’utilité de notre service.

FaqCostsQuestion: Est-ce qu’il y a des coûts lorsque j’utilise le service?
FaqCostsAnswer: Non, ce service est gratuit- les écoutants sont des bénévoles. Avant d’appeler un numéro mobile dans un autre pays, veuillez tenir compte des coûts internationaux et si l’écoutant(e) accepte les appels internationaux.

FaqWorkingsQuestion: Comment ça marche ? Comment je commence?
FaqWorkingsAnswer: Nos écoutants sauront comment commencer. Vous êtes les bienvenus tel que vous êtes.

FaqProblemsQuestion: Comment puis-je savoir si  mes problèmes sont assez importants pour appeler ce service?
FaqProblemsAnswer: Tout ce qui se passe dans votre quotidien durant cette pandémie de Covid19 est bienvenu lorsque vous appelez nos écoutants. Peut-être que vous êtes fatigué, épuisé et submergé  par de nombreuses heures de travail et que vous voulez le dire à quelqu’un avant de rentrer à la maison? Peut-être que quelque chose de grave s’est produit sur votre lieu de travail aujourd’hui ou il y a quelques semaines et que cela vous pèse encore. Peut-être avez-vous l’impression que votre stress a déjà un effet sur votre famille et que vous ne pouvez pas parler de certains sujets liés à votre travail à la maison? Tout ce que vous avez sur votre cœur et qui est connecté à ce que vous vivez au quotidien dans vos fonctions est bienvenu pour être entendu par nos écoutants - quelque soit l’importance que vous lui accordez.

FaqEmpathyQuestion: Qu’est-ce que l’empathie?
FaqEmpathyAnswer1: Nos écoutants pratiquent une forme profonde d’écoute compatissante. Nous l’appelons l’empathie se manifestant à travers un regard positif inconditionnel et créant un espace sans jugement; une autre personne présente à ce qui se passe pour vous, à comment vous vous sentez. Nous sommes tous entraînés à être en présence et à cheminer avec vous là où vous êtes. Bon nombre d’entre nous pratiquent depuis de nombreuses années dans les écoles, les prisons, les hôpitaux, au sein de nos propres familles et certains d’entre nous ont des clients individuels. Voici quelques bénéfices de l’empathie pour les soignants:
FaqEmpathyAnswerList1: Réduire le stress
FaqEmpathyAnswerList2: Préserver les ressources vitales
FaqEmpathyAnswerList3: Prévenir la surcharge et l’épuisement
FaqEmpathyAnswerList4: Favoriser l’auto-régulation, la clarté
FaqEmpathyAnswerList5: Adopter des comportements bénéfiques pour soi-même et les autres
FaqEmpathyAnswer2: Nous souhaitons continuer à bâtir un monde où il y en aura assez pour tous au besoin.

FaqComfortQuestion: Je me sens mal à l’aise à l’idée d’avoir besoin d’aide…
FaqComfortAnswer: Peut-être est-il difficile de demander parce que vous n’en avez pas eu besoin jusqu’à présent? Ou est-ce difficile parce qu’il semble que les autres ont l’air de faire face et de s’en sortir? Partager ce qui ce passe pour vous, comment vous vous sentez, peut être un grand pas à faire;  nous vous encourageons à demander de l’aide, même si c’est quelque chose d’inhabituel ou de nouveau pour vous. Vous et vos besoins sont précieux, aussi précieux que ceux des autres. Vous êtes les bienvenus.

FaqBotheringQuestion: Vais-je déranger l’écoutant ?
FaqBotheringAnswer: Nos écoutants offrent leur présence car cela répond à leur besoin de contribuer. Nous avons confiance que chacun d’entre eux en offrant ce service, sait qu’il en bénéficie aussi. L’une de nos écoutantes nous a dit ceci : « Cela me donne tant de joie et même un sentiment de soulagement de faire partie du projet WEM. Comme je suis chaque jour seule à la maison, je peux à peine imaginer ce que c’est pour vous sur la ligne de front. Je désire être utile pendant cette crise, en proposant un espace d’écoute pour les autres. Cela donne à mes journées un sens et un but... Sachez donc qu’en utilisant le service WEM, vous rendez mes journées encore plus merveilleuses. Je suis là et je désire être là pour les autres, non pas pour vous “secourir”, mais pour être présent à vos côtés, cœur à cœur, humain à humain. »

FaqStrangerQuestion: J’hésite à parler à un étranger…
FaqStrangerAnswer: Peut-être est-il inhabituel de parler et de recevoir du soutien d’un étranger ? Et nous souhaitons faire les choses différemment... Un de nos objectifs est d’unir le monde avec empathie, notre réponse à cette crise est de montrer qu’il est possible de se tendre la main, peut-être d’une manière que nous n’avons jamais essayé auparavant. Si nous nous présentons dans votre centre de santé, dans votre hôpital ou dans votre pharmacie, nous savons que nous recevrons des soins de votre part. Laissez-nous vous souhaiter la même bienvenue. A la fin de l’appel, nous serons plus proches, non plus des étrangers.

FaqTalkingQuestion: En quoi parler m’aidera-t-il?
FaqTalkingAnswer: Peut-être que vous avez l’inquiétude que ce sera une perte de temps? Ou que vous vous allez être submergé par les émotions et ne pourrez pas continuer à faire  votre travail? Nos écoutants sont formés à l’empathie. La plupart des gens disent qu’être entendu de cette façon augmente l’espace à l’intérieur d’eux pour aller de l’avant, pour traiter ce qui leur est arrivé. Nous voulons vous accompagner afin que vous trouvez, préservez vos ressources intérieures et continuez de faire votre travail.

FaqQuote: « À maintes reprises, les gens transcendent les effets paralysants de la douleur psychologique lorsqu’ils ont un contact suffisant avec quelqu’un qui peut les écouter avec empathie. » - Dr. Marshall Rosenberg 

FaqThanks: Merci d’avoir lu jusqu’ici. Ce sont quelques questions que nous avons imaginé dans notre intention de prendre soin. Si d’autres considérations se présentent pour vous, veuillez nous en faire part et nous permettre de comprendre ce qui se passe pour vous. Envoyez-nous un courriel.




# About US
AboutUs: À propos de nous
WhatWeDoTitle: Que faisons-nous?

WhatWeDoPar1: The Worldwide Empathy for Medics (WEM)  www.wem.icu, met en lien le personnel médical et de santé avec  des écoutants empathiques, reconnaissant le besoin urgent de bien-être et de régénération pour les soignants.

WhatWeDoPar2: Face à la pandémie mondiale du Covid-19, nous nous sommes unis, portés par notre conviction qu'être entendu peut être une expérience libératrice et stimulante. C'est pourquoi nous voulons offrir ce qui est en nos moyens à ceux qui travaillent dans les hôpitaux et les cliniques. Notre vision est que les soignants du monde entier puissent trouver un écoutant empathique 24/7, en espérant que cela les aidera à poursuivre leur travail, leurs soins et à rester en bonne santé physique et mentale durant la période actuelle.

WhatWeDoPar3: L’empathie signifie, pour nous, écouter les autres avec tout notre être, sans donner de conseils ni chercher de solution. C’est pourquoi WEM n’offre pas de thérapie ou de conseil, mais des personnes qui écoutent de tout cœur tout ce qui se présente et qui a besoin d’une oreille attentive, des défis et des soucis professionnels aux soucis privés.

WhatWeDoPar4: Ce service est gratuit . Vous pouvez l’utiliser à n’importe quel moment, autant de fois que nécessaire. 

WhoWeAreTitle: Qui sommes-nous?

WhoWeArePar1: WEM est un projet initié par Dr. Luc Peetermans (B), créé et coordonné par Blabla-Foundation VZW (B) en collaboration avec une équipe de soutien internationale. 

BehindTitle: Dans les coulisses

BehindPar1: Notre équipe de coordination est issue d’un réseau international et multi-professionnel composé de formateurs certifiés en Communication Nonviolente (CNV), de psychologues, de facilitateurs de processus, de coaches, d’enseignants, de thérapeutes, de concepteurs et d’administratifs. Nous nous sommes rassemblés, inspirés par nos valeurs communes. Il s’agit notamment de la mutualité, de l’honnêteté, de la liberté de choix et de vie, de l’apprentissage et du leadership avec empathie. Nous avons déjà travaillé ensemble sur des projets tels que Life-Enriching Education Lab, Visfera-Material et des programmes de formation comme Who is training who? et NVC in Lightness and Depth. Si vous voulez en savoir plus sur la certification en CNV, suivez ce lien.

OnThePhoneTitle: Au téléphone 

OnThePhonePar1: Nos écoutant(e)s sont des bénévoles du monde entier. Ils sont des formateurs(trices) certifié(e)s en Communication Nonviolente ou nous ont été personnellement recommandés. C’est une de nos façons de veiller  à la qualité et à la fiabilité de notre service.

WemTeamTitle: Voici l’équipe de coordination de WEM:

