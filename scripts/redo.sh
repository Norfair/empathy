#!/usr/bin/env bash

set -e
set -x


killall empathy || true

cd empathy
empathy &
