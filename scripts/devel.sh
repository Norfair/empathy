#!/usr/bin/env bash

set -e
set -x

nice -n 19 stack install :empathy \
  --file-watch \
  --exec="./scripts/redo.sh" \
  --ghc-options="-freverse-errors -DDEVELOPMENT -O0" \
  --fast \
  --test --no-run-tests \
  $@
