{ empathy }:
{ envname }:
{ lib, pkgs, config, ... }:
with lib;

let
  cfg = config.services.empathy."${envname}";
  concatAttrs = attrList: fold (x: y: x // y) { } attrList;
in
{
  options.services.empathy."${envname}" =
    {
      enable = mkEnableOption "Empathy Services";
      hosts =
        mkOption {
          type = types.listOf (types.string);
          example = [ "wem.icu" ];
          default = [ ];
          description = "The host to serve web requests on";
        };
      port =
        mkOption {
          type = types.int;
          default = 8300;
          example = 8300;
          description = "The port to serve web requests on";
        };
      google-analytics-tracking-id =
        mkOption {
          type = types.nullOr types.string;
          default = null;
          example = "XX-XXXXXXXX-XX";
          description = "The google analytics tracking id";
        };
      google-search-console-verification-id =
        mkOption {
          type = types.nullOr types.string;
          default = null;
          example = "XXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
          description = "The google search console verification id";
        };
      google-tag-manager-id =
        mkOption {
          type = types.nullOr types.string;
          default = null;
          example = "GTM-XXXXXX";
          description = "The google tag manager id";
        };
      backup =
        mkOption {
          type =
            types.submodule {
              options =
                {
                  enable = mkEnableOption "Backup Service";
                };
            };
        };
    };
  config =
    let
      workingDir = "/www/empathy/${envname}/data/";
      databaseFile = workingDir + "empathy.db";
      backupDir = workingDir + "backups/";
      webserver-service =
        let
          unlessNull = o: optionalAttrs (!builtins.isNull o);
        in
        {
          description = "Empathy ${envname} Service";
          wantedBy = [ "multi-user.target" ];
          environment =
            concatAttrs [
              {
                "EMPATHY_PORT" = "${builtins.toString (cfg.port)}";
              }
              (if cfg.hosts == [ ] then { } else {
                "EMPATHY_HOST" = "${head cfg.hosts}";
              })
              (
                unlessNull cfg.google-analytics-tracking-id {
                  "EMPATHY_GOOGLE_ANALYTICS_TRACKING" =
                    "${cfg.google-analytics-tracking-id}";
                }
              )
              (
                unlessNull cfg.google-search-console-verification-id {
                  "EMPATHY_GOOGLE_SEARCH_CONSOLE_VERIFICATION" =
                    "${cfg.google-search-console-verification-id}";
                }
              )
              (
                unlessNull cfg.google-tag-manager-id {
                  "EMPATHY_GOOGLE_TAG_MANAGER" =
                    "${cfg.google-tag-manager-id}";
                }
              )
            ];
          script =
            ''
              mkdir -p "${workingDir}"
              cd "${workingDir}"

              mkdir -p ${backupDir}
              file="${backupDir}deploy-''$(date +%F_%T).db"
              ${pkgs.sqlite}/bin/sqlite3 ${databaseFile} ".backup ''${file}"

              ${empathy}/bin/empathy
            '';
          serviceConfig =
            {
              Restart = "always";
              RestartSec = 1;
              Nice = 15;
            };
          unitConfig =
            {
              StartLimitIntervalSec = 0;
              # ensure Restart=always is always honoured
            };
        };
      backup-service =
        {
          description = "Backup database for ${envname}";
          wantedBy = [ ];
          script =
            ''
              mkdir -p ${backupDir}
              file="${backupDir}''$(date +%F_%T).db"
              ${pkgs.sqlite}/bin/sqlite3 ${databaseFile} ".backup ''${file}"
            '';
          serviceConfig =
            {
              Type = "oneshot";
            };
        };
      backup-timer =
        {
          description =
            "Backup database for ${envname} every twelve hours.";
          wantedBy = [ "timers.target" ];
          timerConfig =
            {
              OnCalendar = "00/12:00";
              Persistent = true;
            };
        };

    in
    mkIf cfg.enable {
      systemd.services =
        {
          "empathy-${envname}" = webserver-service;
          "empathy-${envname}-backup" = backup-service;
        };
      systemd.timers =
        { "empathy-${envname}-backup" = backup-timer; };
      networking.firewall.allowedTCPPorts = [ cfg.port ];
      services.nginx.virtualHosts =
        {
          "${head (cfg.hosts)}" =
            {
              enableACME = true;
              forceSSL = true;
              locations."/".proxyPass =
                "http://localhost:${builtins.toString (cfg.port)}";
              serverAliases = tail cfg.hosts;
            };
        };
    };
}
