final: prev:
with final.lib;
with final.haskell.lib;

{
  empathy = justStaticExecutables final.haskellPackages.empathy;
  haskellPackages =
    prev.haskellPackages.override (old: {
      overrides = composeExtensions (old.overrides or (_: _: { })) (
        self: super: {
          empathy = buildStrictly (self.callPackage ../empathy { });
        }
      );
    });
}
