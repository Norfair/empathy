{ nixosTest
, empathy-nixos-module-factory
}:
let
  empathy-production = empathy-nixos-module-factory {
    envname = "production";
  };
  port = 8001;
in
nixosTest (
  { lib, pkgs, ... }: {
    name = "empathy-module-test";
    nodes = {
      server = {
        imports = [
          empathy-production
        ];
        services.empathy.production = {
          enable = true;
          inherit port;
        };
      };
    };
    testScript = ''
      server.start()
      server.wait_for_unit("multi-user.target")

      server.wait_for_open_port(${builtins.toString port})
      server.succeed("curl localhost:${builtins.toString port}")
    '';
  }
)
